#pragma once

#include "sender.h"



namespace F103BootloaderDesktop {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;
	using namespace System::Configuration;
	using namespace System::Threading;

	/// <summary>
	/// Create the MainForm
	/// </summary>
	public ref class MainForm : public System::Windows::Forms::Form
	{
	public:
		MainForm(void)
		{
			InitializeComponent();
			findAndPrintComPorts();
			loadUARTsettings();
			f103_connected = false;
			valid_file = false;
			valid_file_encrypt = false;
			connectMode = false;
			sendMode = false;
			mySender = gcnew Sender(this->serialPort);

			// Save the textboxes for the password in an array
			textBoxPasswordArray[0] = this->textBoxP0;
			textBoxPasswordArray[1] = this->textBoxP1;
			textBoxPasswordArray[2] = this->textBoxP2;
			textBoxPasswordArray[3] = this->textBoxP3;
			textBoxPasswordArray[4] = this->textBoxP4;
			textBoxPasswordArray[5] = this->textBoxP5;
			textBoxPasswordArray[6] = this->textBoxP6;
			textBoxPasswordArray[7] = this->textBoxP7;
			textBoxPasswordArray[8] = this->textBoxP8;
			textBoxPasswordArray[9] = this->textBoxP9;
			textBoxPasswordArray[10] = this->textBoxP10;
			textBoxPasswordArray[11] = this->textBoxP11;
			textBoxPasswordArray[12] = this->textBoxP12;
			textBoxPasswordArray[13] = this->textBoxP13;
			textBoxPasswordArray[14] = this->textBoxP14;
			textBoxPasswordArray[15] = this->textBoxP15;

			// Prevent to tab through radiobuttons while pressing tab
			this->radioButtonArray->TabStop = false;
			this->radioButtonString->TabStop = false;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MainForm()
		{
			if (components)
			{
				delete components;
			}
		}

		// Object for TabControl
	private: System::Windows::Forms::TabControl^ tabControl;
	private: System::Windows::Forms::TabPage^ tabPageSend;
	private: System::Windows::Forms::TabPage^ tabPageEncrypt;


		   // Objects for sending
	private: System::Windows::Forms::GroupBox^ groupBoxUART;
	private: System::Windows::Forms::Button^ connectUartBTN;
	private: System::Windows::Forms::ComboBox^ comboBoxRate;
	private: System::Windows::Forms::Label^ labelBaud;
	private: System::Windows::Forms::Button^ refreshPortsBtn;
	private: System::Windows::Forms::ComboBox^ comboBoxPort;
	private: System::Windows::Forms::Label^ labelPort;
	private: System::ComponentModel::IContainer^ components;
	private: System::IO::Ports::SerialPort^ serialPort;
	private: System::Windows::Forms::Label^ labelStatusUart;
	private: System::Windows::Forms::Label^ labelStatusText;
	private: System::Windows::Forms::Label^ labelBootloaderVersionNumber;
	private: System::Windows::Forms::Label^ labelBootloaderVersion;

	private: System::Windows::Forms::GroupBox^ groupBoxFile;
	private: System::Windows::Forms::Button^ selectFileBtn;
	private: System::Windows::Forms::TextBox^ filePathText;
	private: System::Windows::Forms::OpenFileDialog^ fileDialog;
	private: System::Windows::Forms::Label^ labelFileStatus;
	private: System::Windows::Forms::TextBox^ textBoxAddress;
	private: System::Windows::Forms::Label^ labelAddress;

	private: System::Windows::Forms::GroupBox^ groupBoxSend;
	private: System::Windows::Forms::CheckBox^ checkBoxAsync;
	private: System::Windows::Forms::Button^ OpenFileBtn;
	private: System::Windows::Forms::Button^ sendBtn;
	private: System::Windows::Forms::TextBox^ textBoxResult;
	private: System::Windows::Forms::RichTextBox^ richTextBoxHex;


		   // Objects for encryption
	private: System::Windows::Forms::GroupBox^ groupBoxSelectFileEncrypt;
	private: System::Windows::Forms::Button^ encryptFileBtn;
	private: System::Windows::Forms::Label^ labelFileStatusEncrypt;
	private: System::Windows::Forms::TextBox^ filePathEncryptText;
	private: System::Windows::Forms::TextBox^ textBoxAddressEncrypt;
	private: System::Windows::Forms::Button^ selectFileEncryptBtn;
	private: System::Windows::Forms::Label^ labelAddressEncrypt;
	private: System::Windows::Forms::OpenFileDialog^ fileDialogEncrypt;
	private: System::Windows::Forms::TextBox^ textBoxPacketSize;
	private: System::Windows::Forms::Label^ labelPacketSize;
	private: System::Windows::Forms::Label^ labelSelectedStartAddressNumber;
	private: System::Windows::Forms::Label^ labelSelectedStartAddress;
	private: System::Windows::Forms::Label^ labelSelectedPacketSizeNumber;
	private: System::Windows::Forms::Label^ labelSelectedPacketSize;
	private: System::Windows::Forms::Label^ labelReceivedPacketSizeNumber;
	private: System::Windows::Forms::Label^ labelReceivedPacketSize;

	private: System::Windows::Forms::GroupBox^ groupBoxFileEncrypt;
	private: System::Windows::Forms::RadioButton^ radioButtonArray;
	private: System::Windows::Forms::RadioButton^ radioButtonString;
	private: System::Windows::Forms::Label^ labelPassword16;
	private: System::Windows::Forms::TextBox^ textBoxPassword;
	private: System::Windows::Forms::TextBox^ textBoxResultEncrypt;
	private: System::Windows::Forms::Label^ labelResultEncrypt;

	private: System::Windows::Forms::GroupBox^ groupBoxPassword;
	private: System::Windows::Forms::TextBox^ textBoxP15;
	private: System::Windows::Forms::TextBox^ textBoxP14;
	private: System::Windows::Forms::TextBox^ textBoxP13;
	private: System::Windows::Forms::TextBox^ textBoxP12;
	private: System::Windows::Forms::TextBox^ textBoxP11;
	private: System::Windows::Forms::TextBox^ textBoxP10;
	private: System::Windows::Forms::TextBox^ textBoxP9;
	private: System::Windows::Forms::TextBox^ textBoxP8;
	private: System::Windows::Forms::TextBox^ textBoxP7;
	private: System::Windows::Forms::TextBox^ textBoxP6;
	private: System::Windows::Forms::TextBox^ textBoxP5;
	private: System::Windows::Forms::TextBox^ textBoxP4;
	private: System::Windows::Forms::TextBox^ textBoxP3;
	private: System::Windows::Forms::TextBox^ textBoxP2;
	private: System::Windows::Forms::TextBox^ textBoxP1;
	private: System::Windows::Forms::TextBox^ textBoxP0;
	private: System::Windows::Forms::Label^ labelP15;
	private: System::Windows::Forms::Label^ labelP14;
	private: System::Windows::Forms::Label^ labelP13;
	private: System::Windows::Forms::Label^ labelP12;
	private: System::Windows::Forms::Label^ labelP11;
	private: System::Windows::Forms::Label^ labelP10;
	private: System::Windows::Forms::Label^ labelP9;
	private: System::Windows::Forms::Label^ labelP8;
	private: System::Windows::Forms::Label^ labelP7;
	private: System::Windows::Forms::Label^ labelP6;
	private: System::Windows::Forms::Label^ labelP5;
	private: System::Windows::Forms::Label^ labelP4;
	private: System::Windows::Forms::Label^ labelP3;
	private: System::Windows::Forms::Label^ labelP2;
	private: System::Windows::Forms::Label^ labelP1;
	private: System::Windows::Forms::Label^ labelP0;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		bool f103_connected;	//Indicates if the desktop application is connected to a Bootloader
		bool valid_file;		//Indicates if a valid file for sending was selected
		bool valid_file_encrypt;
		bool connectMode;		//Indicates if the desktop application is currently trying to connect to the bootloader
		bool sendMode;			//Indicates if the desktop application is currently sending a file to the bootloader
		delegate void SafeCallDelegate(bool status);	//Delegate needed to change values from the MainForm out of a other Thread
		Sender^ mySender;		//Object which sends the files to the STM32F103
		Thread^ connectThread;	//Thread for conneting to STM32F103
		Thread^ sendThread;		//Thread for sennding a file to STM32F103

		// Array of textboxes, used for the textboxes which contain the bytes of the password for encryption
		cli::array<System::Windows::Forms::TextBox^>^ textBoxPasswordArray = gcnew cli::array<System::Windows::Forms::TextBox^>(16);

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^ resources = (gcnew System::ComponentModel::ComponentResourceManager(MainForm::typeid));
			this->groupBoxUART = (gcnew System::Windows::Forms::GroupBox());
			this->labelReceivedPacketSizeNumber = (gcnew System::Windows::Forms::Label());
			this->labelReceivedPacketSize = (gcnew System::Windows::Forms::Label());
			this->labelBootloaderVersionNumber = (gcnew System::Windows::Forms::Label());
			this->labelBootloaderVersion = (gcnew System::Windows::Forms::Label());
			this->labelStatusUart = (gcnew System::Windows::Forms::Label());
			this->labelStatusText = (gcnew System::Windows::Forms::Label());
			this->connectUartBTN = (gcnew System::Windows::Forms::Button());
			this->comboBoxRate = (gcnew System::Windows::Forms::ComboBox());
			this->labelBaud = (gcnew System::Windows::Forms::Label());
			this->refreshPortsBtn = (gcnew System::Windows::Forms::Button());
			this->comboBoxPort = (gcnew System::Windows::Forms::ComboBox());
			this->labelPort = (gcnew System::Windows::Forms::Label());
			this->textBoxResult = (gcnew System::Windows::Forms::TextBox());
			this->serialPort = (gcnew System::IO::Ports::SerialPort(this->components));
			this->groupBoxFile = (gcnew System::Windows::Forms::GroupBox());
			this->textBoxAddress = (gcnew System::Windows::Forms::TextBox());
			this->labelAddress = (gcnew System::Windows::Forms::Label());
			this->labelFileStatus = (gcnew System::Windows::Forms::Label());
			this->selectFileBtn = (gcnew System::Windows::Forms::Button());
			this->filePathText = (gcnew System::Windows::Forms::TextBox());
			this->fileDialog = (gcnew System::Windows::Forms::OpenFileDialog());
			this->richTextBoxHex = (gcnew System::Windows::Forms::RichTextBox());
			this->OpenFileBtn = (gcnew System::Windows::Forms::Button());
			this->sendBtn = (gcnew System::Windows::Forms::Button());
			this->groupBoxSend = (gcnew System::Windows::Forms::GroupBox());
			this->checkBoxAsync = (gcnew System::Windows::Forms::CheckBox());
			this->tabControl = (gcnew System::Windows::Forms::TabControl());
			this->tabPageSend = (gcnew System::Windows::Forms::TabPage());
			this->tabPageEncrypt = (gcnew System::Windows::Forms::TabPage());
			this->groupBoxFileEncrypt = (gcnew System::Windows::Forms::GroupBox());
			this->labelSelectedStartAddressNumber = (gcnew System::Windows::Forms::Label());
			this->labelSelectedStartAddress = (gcnew System::Windows::Forms::Label());
			this->labelSelectedPacketSizeNumber = (gcnew System::Windows::Forms::Label());
			this->labelSelectedPacketSize = (gcnew System::Windows::Forms::Label());
			this->labelResultEncrypt = (gcnew System::Windows::Forms::Label());
			this->groupBoxPassword = (gcnew System::Windows::Forms::GroupBox());
			this->textBoxP15 = (gcnew System::Windows::Forms::TextBox());
			this->labelP0 = (gcnew System::Windows::Forms::Label());
			this->labelP2 = (gcnew System::Windows::Forms::Label());
			this->labelP15 = (gcnew System::Windows::Forms::Label());
			this->textBoxP14 = (gcnew System::Windows::Forms::TextBox());
			this->labelP3 = (gcnew System::Windows::Forms::Label());
			this->labelP4 = (gcnew System::Windows::Forms::Label());
			this->textBoxP13 = (gcnew System::Windows::Forms::TextBox());
			this->labelP5 = (gcnew System::Windows::Forms::Label());
			this->textBoxP12 = (gcnew System::Windows::Forms::TextBox());
			this->labelP6 = (gcnew System::Windows::Forms::Label());
			this->labelP1 = (gcnew System::Windows::Forms::Label());
			this->textBoxP11 = (gcnew System::Windows::Forms::TextBox());
			this->labelP7 = (gcnew System::Windows::Forms::Label());
			this->labelP14 = (gcnew System::Windows::Forms::Label());
			this->textBoxP10 = (gcnew System::Windows::Forms::TextBox());
			this->textBoxP0 = (gcnew System::Windows::Forms::TextBox());
			this->textBoxP4 = (gcnew System::Windows::Forms::TextBox());
			this->textBoxP5 = (gcnew System::Windows::Forms::TextBox());
			this->textBoxP9 = (gcnew System::Windows::Forms::TextBox());
			this->textBoxP3 = (gcnew System::Windows::Forms::TextBox());
			this->textBoxP8 = (gcnew System::Windows::Forms::TextBox());
			this->textBoxP6 = (gcnew System::Windows::Forms::TextBox());
			this->textBoxP2 = (gcnew System::Windows::Forms::TextBox());
			this->labelP13 = (gcnew System::Windows::Forms::Label());
			this->textBoxP7 = (gcnew System::Windows::Forms::TextBox());
			this->textBoxP1 = (gcnew System::Windows::Forms::TextBox());
			this->labelP8 = (gcnew System::Windows::Forms::Label());
			this->labelP9 = (gcnew System::Windows::Forms::Label());
			this->labelP12 = (gcnew System::Windows::Forms::Label());
			this->labelP10 = (gcnew System::Windows::Forms::Label());
			this->labelP11 = (gcnew System::Windows::Forms::Label());
			this->textBoxResultEncrypt = (gcnew System::Windows::Forms::TextBox());
			this->radioButtonArray = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonString = (gcnew System::Windows::Forms::RadioButton());
			this->labelPassword16 = (gcnew System::Windows::Forms::Label());
			this->textBoxPassword = (gcnew System::Windows::Forms::TextBox());
			this->encryptFileBtn = (gcnew System::Windows::Forms::Button());
			this->groupBoxSelectFileEncrypt = (gcnew System::Windows::Forms::GroupBox());
			this->textBoxPacketSize = (gcnew System::Windows::Forms::TextBox());
			this->labelPacketSize = (gcnew System::Windows::Forms::Label());
			this->labelFileStatusEncrypt = (gcnew System::Windows::Forms::Label());
			this->filePathEncryptText = (gcnew System::Windows::Forms::TextBox());
			this->textBoxAddressEncrypt = (gcnew System::Windows::Forms::TextBox());
			this->selectFileEncryptBtn = (gcnew System::Windows::Forms::Button());
			this->labelAddressEncrypt = (gcnew System::Windows::Forms::Label());
			this->fileDialogEncrypt = (gcnew System::Windows::Forms::OpenFileDialog());
			this->groupBoxUART->SuspendLayout();
			this->groupBoxFile->SuspendLayout();
			this->groupBoxSend->SuspendLayout();
			this->tabControl->SuspendLayout();
			this->tabPageSend->SuspendLayout();
			this->tabPageEncrypt->SuspendLayout();
			this->groupBoxFileEncrypt->SuspendLayout();
			this->groupBoxPassword->SuspendLayout();
			this->groupBoxSelectFileEncrypt->SuspendLayout();
			this->SuspendLayout();
			// 
			// groupBoxUART
			// 
			this->groupBoxUART->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBoxUART->BackColor = System::Drawing::SystemColors::GradientActiveCaption;
			this->groupBoxUART->Controls->Add(this->labelReceivedPacketSizeNumber);
			this->groupBoxUART->Controls->Add(this->labelReceivedPacketSize);
			this->groupBoxUART->Controls->Add(this->labelBootloaderVersionNumber);
			this->groupBoxUART->Controls->Add(this->labelBootloaderVersion);
			this->groupBoxUART->Controls->Add(this->labelStatusUart);
			this->groupBoxUART->Controls->Add(this->labelStatusText);
			this->groupBoxUART->Controls->Add(this->connectUartBTN);
			this->groupBoxUART->Controls->Add(this->comboBoxRate);
			this->groupBoxUART->Controls->Add(this->labelBaud);
			this->groupBoxUART->Controls->Add(this->refreshPortsBtn);
			this->groupBoxUART->Controls->Add(this->comboBoxPort);
			this->groupBoxUART->Controls->Add(this->labelPort);
			this->groupBoxUART->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->groupBoxUART->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->groupBoxUART->Location = System::Drawing::Point(3, 2);
			this->groupBoxUART->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->groupBoxUART->Name = L"groupBoxUART";
			this->groupBoxUART->Padding = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->groupBoxUART->Size = System::Drawing::Size(891, 169);
			this->groupBoxUART->TabIndex = 0;
			this->groupBoxUART->TabStop = false;
			this->groupBoxUART->Text = L"UART settings";
			// 
			// labelReceivedPacketSizeNumber
			// 
			this->labelReceivedPacketSizeNumber->AutoSize = true;
			this->labelReceivedPacketSizeNumber->Location = System::Drawing::Point(829, 134);
			this->labelReceivedPacketSizeNumber->Name = L"labelReceivedPacketSizeNumber";
			this->labelReceivedPacketSizeNumber->Size = System::Drawing::Size(23, 18);
			this->labelReceivedPacketSizeNumber->TabIndex = 12;
			this->labelReceivedPacketSizeNumber->Text = L"---";
			// 
			// labelReceivedPacketSize
			// 
			this->labelReceivedPacketSize->AutoSize = true;
			this->labelReceivedPacketSize->Location = System::Drawing::Point(644, 134);
			this->labelReceivedPacketSize->Name = L"labelReceivedPacketSize";
			this->labelReceivedPacketSize->Size = System::Drawing::Size(164, 18);
			this->labelReceivedPacketSize->TabIndex = 11;
			this->labelReceivedPacketSize->Text = L"Packet size for bin files:";
			// 
			// labelBootloaderVersionNumber
			// 
			this->labelBootloaderVersionNumber->AutoSize = true;
			this->labelBootloaderVersionNumber->Location = System::Drawing::Point(829, 98);
			this->labelBootloaderVersionNumber->Name = L"labelBootloaderVersionNumber";
			this->labelBootloaderVersionNumber->Size = System::Drawing::Size(23, 18);
			this->labelBootloaderVersionNumber->TabIndex = 10;
			this->labelBootloaderVersionNumber->Text = L"---";
			// 
			// labelBootloaderVersion
			// 
			this->labelBootloaderVersion->AutoSize = true;
			this->labelBootloaderVersion->Location = System::Drawing::Point(644, 98);
			this->labelBootloaderVersion->Name = L"labelBootloaderVersion";
			this->labelBootloaderVersion->Size = System::Drawing::Size(139, 18);
			this->labelBootloaderVersion->TabIndex = 9;
			this->labelBootloaderVersion->Text = L"Bootloader Version:";
			// 
			// labelStatusUart
			// 
			this->labelStatusUart->AutoSize = true;
			this->labelStatusUart->Location = System::Drawing::Point(96, 92);
			this->labelStatusUart->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->labelStatusUart->Name = L"labelStatusUart";
			this->labelStatusUart->Size = System::Drawing::Size(102, 18);
			this->labelStatusUart->TabIndex = 8;
			this->labelStatusUart->Text = L"not connected";
			// 
			// labelStatusText
			// 
			this->labelStatusText->AutoSize = true;
			this->labelStatusText->Location = System::Drawing::Point(15, 92);
			this->labelStatusText->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->labelStatusText->Name = L"labelStatusText";
			this->labelStatusText->Size = System::Drawing::Size(54, 18);
			this->labelStatusText->TabIndex = 7;
			this->labelStatusText->Text = L"Status:";
			// 
			// connectUartBTN
			// 
			this->connectUartBTN->BackColor = System::Drawing::SystemColors::Control;
			this->connectUartBTN->FlatAppearance->BorderSize = 2;
			this->connectUartBTN->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->connectUartBTN->Location = System::Drawing::Point(759, 33);
			this->connectUartBTN->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->connectUartBTN->Name = L"connectUartBTN";
			this->connectUartBTN->Size = System::Drawing::Size(115, 49);
			this->connectUartBTN->TabIndex = 5;
			this->connectUartBTN->TabStop = false;
			this->connectUartBTN->Text = L"Connect\r\n";
			this->connectUartBTN->UseVisualStyleBackColor = false;
			this->connectUartBTN->Click += gcnew System::EventHandler(this, &MainForm::connectUartBtn_Click);
			// 
			// comboBoxRate
			// 
			this->comboBoxRate->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBoxRate->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->comboBoxRate->FormattingEnabled = true;
			this->comboBoxRate->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"115200", L"9600" });
			this->comboBoxRate->Location = System::Drawing::Point(481, 44);
			this->comboBoxRate->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->comboBoxRate->Name = L"comboBoxRate";
			this->comboBoxRate->Size = System::Drawing::Size(124, 26);
			this->comboBoxRate->TabIndex = 4;
			// 
			// labelBaud
			// 
			this->labelBaud->AutoSize = true;
			this->labelBaud->Location = System::Drawing::Point(403, 47);
			this->labelBaud->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->labelBaud->Name = L"labelBaud";
			this->labelBaud->Size = System::Drawing::Size(71, 18);
			this->labelBaud->TabIndex = 3;
			this->labelBaud->Text = L"Baud rate";
			// 
			// refreshPortsBtn
			// 
			this->refreshPortsBtn->BackColor = System::Drawing::SystemColors::Control;
			this->refreshPortsBtn->FlatAppearance->BorderSize = 2;
			this->refreshPortsBtn->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->refreshPortsBtn->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"refreshPortsBtn.Image")));
			this->refreshPortsBtn->Location = System::Drawing::Point(235, 33);
			this->refreshPortsBtn->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->refreshPortsBtn->Name = L"refreshPortsBtn";
			this->refreshPortsBtn->Size = System::Drawing::Size(68, 49);
			this->refreshPortsBtn->TabIndex = 2;
			this->refreshPortsBtn->TabStop = false;
			this->refreshPortsBtn->UseVisualStyleBackColor = false;
			this->refreshPortsBtn->Click += gcnew System::EventHandler(this, &MainForm::refreshPortsBtn_Click);
			// 
			// comboBoxPort
			// 
			this->comboBoxPort->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBoxPort->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->comboBoxPort->FormattingEnabled = true;
			this->comboBoxPort->Location = System::Drawing::Point(99, 44);
			this->comboBoxPort->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->comboBoxPort->Name = L"comboBoxPort";
			this->comboBoxPort->Size = System::Drawing::Size(109, 26);
			this->comboBoxPort->TabIndex = 1;
			// 
			// labelPort
			// 
			this->labelPort->AutoSize = true;
			this->labelPort->Location = System::Drawing::Point(15, 48);
			this->labelPort->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->labelPort->Name = L"labelPort";
			this->labelPort->Size = System::Drawing::Size(76, 18);
			this->labelPort->TabIndex = 0;
			this->labelPort->Text = L"COM Port";
			// 
			// textBoxResult
			// 
			this->textBoxResult->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->textBoxResult->Location = System::Drawing::Point(615, 180);
			this->textBoxResult->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->textBoxResult->Name = L"textBoxResult";
			this->textBoxResult->ReadOnly = true;
			this->textBoxResult->Size = System::Drawing::Size(263, 24);
			this->textBoxResult->TabIndex = 1;
			this->textBoxResult->TabStop = false;
			// 
			// groupBoxFile
			// 
			this->groupBoxFile->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBoxFile->BackColor = System::Drawing::SystemColors::GradientActiveCaption;
			this->groupBoxFile->Controls->Add(this->textBoxAddress);
			this->groupBoxFile->Controls->Add(this->labelAddress);
			this->groupBoxFile->Controls->Add(this->labelFileStatus);
			this->groupBoxFile->Controls->Add(this->selectFileBtn);
			this->groupBoxFile->Controls->Add(this->filePathText);
			this->groupBoxFile->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->groupBoxFile->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->groupBoxFile->Location = System::Drawing::Point(3, 172);
			this->groupBoxFile->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->groupBoxFile->Name = L"groupBoxFile";
			this->groupBoxFile->Padding = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->groupBoxFile->Size = System::Drawing::Size(891, 167);
			this->groupBoxFile->TabIndex = 2;
			this->groupBoxFile->TabStop = false;
			this->groupBoxFile->Text = L"Select file";
			// 
			// textBoxAddress
			// 
			this->textBoxAddress->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->textBoxAddress->Location = System::Drawing::Point(729, 128);
			this->textBoxAddress->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxAddress->Name = L"textBoxAddress";
			this->textBoxAddress->Size = System::Drawing::Size(139, 24);
			this->textBoxAddress->TabIndex = 4;
			// 
			// labelAddress
			// 
			this->labelAddress->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->labelAddress->AutoSize = true;
			this->labelAddress->Location = System::Drawing::Point(353, 128);
			this->labelAddress->Name = L"labelAddress";
			this->labelAddress->Size = System::Drawing::Size(315, 18);
			this->labelAddress->TabIndex = 3;
			this->labelAddress->Text = L"Start address for bin file in hexadecimal format:";
			// 
			// labelFileStatus
			// 
			this->labelFileStatus->AutoSize = true;
			this->labelFileStatus->Location = System::Drawing::Point(13, 82);
			this->labelFileStatus->Name = L"labelFileStatus";
			this->labelFileStatus->Size = System::Drawing::Size(109, 18);
			this->labelFileStatus->TabIndex = 2;
			this->labelFileStatus->Text = L"No file selected";
			// 
			// selectFileBtn
			// 
			this->selectFileBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->selectFileBtn->BackColor = System::Drawing::SystemColors::Control;
			this->selectFileBtn->FlatAppearance->BorderSize = 2;
			this->selectFileBtn->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->selectFileBtn->Location = System::Drawing::Point(759, 31);
			this->selectFileBtn->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->selectFileBtn->Name = L"selectFileBtn";
			this->selectFileBtn->Size = System::Drawing::Size(115, 49);
			this->selectFileBtn->TabIndex = 1;
			this->selectFileBtn->TabStop = false;
			this->selectFileBtn->Text = L"Select file";
			this->selectFileBtn->UseVisualStyleBackColor = false;
			this->selectFileBtn->Click += gcnew System::EventHandler(this, &MainForm::selectFileBtn_Click);
			// 
			// filePathText
			// 
			this->filePathText->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->filePathText->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
			this->filePathText->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::FileSystem;
			this->filePathText->Location = System::Drawing::Point(17, 42);
			this->filePathText->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->filePathText->Name = L"filePathText";
			this->filePathText->Size = System::Drawing::Size(685, 24);
			this->filePathText->TabIndex = 0;
			this->filePathText->TextChanged += gcnew System::EventHandler(this, &MainForm::filePathText_TextChanged);
			// 
			// fileDialog
			// 
			this->fileDialog->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &MainForm::fileDialog_FileOk);
			// 
			// richTextBoxHex
			// 
			this->richTextBoxHex->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->richTextBoxHex->Font = (gcnew System::Drawing::Font(L"Lucida Console", 10.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->richTextBoxHex->Location = System::Drawing::Point(17, 42);
			this->richTextBoxHex->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->richTextBoxHex->Name = L"richTextBoxHex";
			this->richTextBoxHex->ReadOnly = true;
			this->richTextBoxHex->Size = System::Drawing::Size(589, 222);
			this->richTextBoxHex->TabIndex = 3;
			this->richTextBoxHex->TabStop = false;
			this->richTextBoxHex->Text = L"";
			// 
			// OpenFileBtn
			// 
			this->OpenFileBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->OpenFileBtn->BackColor = System::Drawing::SystemColors::Control;
			this->OpenFileBtn->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->OpenFileBtn->Location = System::Drawing::Point(615, 42);
			this->OpenFileBtn->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->OpenFileBtn->Name = L"OpenFileBtn";
			this->OpenFileBtn->Size = System::Drawing::Size(115, 49);
			this->OpenFileBtn->TabIndex = 4;
			this->OpenFileBtn->TabStop = false;
			this->OpenFileBtn->Text = L"View file";
			this->OpenFileBtn->UseVisualStyleBackColor = false;
			this->OpenFileBtn->Click += gcnew System::EventHandler(this, &MainForm::OpenFileBtn_Click);
			// 
			// sendBtn
			// 
			this->sendBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->sendBtn->BackColor = System::Drawing::SystemColors::Control;
			this->sendBtn->FlatAppearance->BorderSize = 2;
			this->sendBtn->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->sendBtn->Location = System::Drawing::Point(759, 42);
			this->sendBtn->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->sendBtn->Name = L"sendBtn";
			this->sendBtn->Size = System::Drawing::Size(115, 49);
			this->sendBtn->TabIndex = 5;
			this->sendBtn->TabStop = false;
			this->sendBtn->Text = L"Send file";
			this->sendBtn->UseVisualStyleBackColor = false;
			this->sendBtn->Click += gcnew System::EventHandler(this, &MainForm::sendBtn_Click);
			// 
			// groupBoxSend
			// 
			this->groupBoxSend->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBoxSend->BackColor = System::Drawing::SystemColors::GradientActiveCaption;
			this->groupBoxSend->Controls->Add(this->checkBoxAsync);
			this->groupBoxSend->Controls->Add(this->textBoxResult);
			this->groupBoxSend->Controls->Add(this->OpenFileBtn);
			this->groupBoxSend->Controls->Add(this->sendBtn);
			this->groupBoxSend->Controls->Add(this->richTextBoxHex);
			this->groupBoxSend->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->groupBoxSend->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->groupBoxSend->Location = System::Drawing::Point(-1, 345);
			this->groupBoxSend->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->groupBoxSend->Name = L"groupBoxSend";
			this->groupBoxSend->Padding = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->groupBoxSend->Size = System::Drawing::Size(891, 299);
			this->groupBoxSend->TabIndex = 7;
			this->groupBoxSend->TabStop = false;
			this->groupBoxSend->Text = L"View and send file";
			// 
			// checkBoxAsync
			// 
			this->checkBoxAsync->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->checkBoxAsync->AutoSize = true;
			this->checkBoxAsync->Checked = true;
			this->checkBoxAsync->CheckState = System::Windows::Forms::CheckState::Checked;
			this->checkBoxAsync->Location = System::Drawing::Point(750, 124);
			this->checkBoxAsync->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->checkBoxAsync->Name = L"checkBoxAsync";
			this->checkBoxAsync->Size = System::Drawing::Size(124, 40);
			this->checkBoxAsync->TabIndex = 7;
			this->checkBoxAsync->TabStop = false;
			this->checkBoxAsync->Text = L"Send\r\nasynchronous";
			this->checkBoxAsync->UseVisualStyleBackColor = true;
			// 
			// tabControl
			// 
			this->tabControl->Controls->Add(this->tabPageSend);
			this->tabControl->Controls->Add(this->tabPageEncrypt);
			this->tabControl->Dock = System::Windows::Forms::DockStyle::Fill;
			this->tabControl->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->tabControl->ItemSize = System::Drawing::Size(500, 50);
			this->tabControl->Location = System::Drawing::Point(0, 0);
			this->tabControl->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->tabControl->Name = L"tabControl";
			this->tabControl->Padding = System::Drawing::Point(50, 3);
			this->tabControl->SelectedIndex = 0;
			this->tabControl->Size = System::Drawing::Size(905, 703);
			this->tabControl->TabIndex = 29;
			this->tabControl->TabStop = false;
			this->tabControl->SelectedIndexChanged += gcnew System::EventHandler(this, &MainForm::tabControl_SelectedIndexChanged);
			// 
			// tabPageSend
			// 
			this->tabPageSend->BackColor = System::Drawing::SystemColors::GradientActiveCaption;
			this->tabPageSend->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
			this->tabPageSend->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->tabPageSend->Controls->Add(this->groupBoxSend);
			this->tabPageSend->Controls->Add(this->groupBoxFile);
			this->tabPageSend->Controls->Add(this->groupBoxUART);
			this->tabPageSend->Location = System::Drawing::Point(4, 54);
			this->tabPageSend->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->tabPageSend->Name = L"tabPageSend";
			this->tabPageSend->Padding = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->tabPageSend->Size = System::Drawing::Size(897, 645);
			this->tabPageSend->TabIndex = 0;
			this->tabPageSend->Text = L"Send file";
			// 
			// tabPageEncrypt
			// 
			this->tabPageEncrypt->BackColor = System::Drawing::SystemColors::GradientActiveCaption;
			this->tabPageEncrypt->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->tabPageEncrypt->Controls->Add(this->groupBoxFileEncrypt);
			this->tabPageEncrypt->Controls->Add(this->groupBoxSelectFileEncrypt);
			this->tabPageEncrypt->Location = System::Drawing::Point(4, 54);
			this->tabPageEncrypt->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->tabPageEncrypt->Name = L"tabPageEncrypt";
			this->tabPageEncrypt->Padding = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->tabPageEncrypt->Size = System::Drawing::Size(897, 645);
			this->tabPageEncrypt->TabIndex = 1;
			this->tabPageEncrypt->Text = L"Encrypt file";
			// 
			// groupBoxFileEncrypt
			// 
			this->groupBoxFileEncrypt->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBoxFileEncrypt->BackColor = System::Drawing::SystemColors::GradientActiveCaption;
			this->groupBoxFileEncrypt->Controls->Add(this->labelSelectedStartAddressNumber);
			this->groupBoxFileEncrypt->Controls->Add(this->labelSelectedStartAddress);
			this->groupBoxFileEncrypt->Controls->Add(this->labelSelectedPacketSizeNumber);
			this->groupBoxFileEncrypt->Controls->Add(this->labelSelectedPacketSize);
			this->groupBoxFileEncrypt->Controls->Add(this->labelResultEncrypt);
			this->groupBoxFileEncrypt->Controls->Add(this->groupBoxPassword);
			this->groupBoxFileEncrypt->Controls->Add(this->textBoxResultEncrypt);
			this->groupBoxFileEncrypt->Controls->Add(this->radioButtonArray);
			this->groupBoxFileEncrypt->Controls->Add(this->radioButtonString);
			this->groupBoxFileEncrypt->Controls->Add(this->labelPassword16);
			this->groupBoxFileEncrypt->Controls->Add(this->textBoxPassword);
			this->groupBoxFileEncrypt->Controls->Add(this->encryptFileBtn);
			this->groupBoxFileEncrypt->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->groupBoxFileEncrypt->Location = System::Drawing::Point(3, 177);
			this->groupBoxFileEncrypt->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->groupBoxFileEncrypt->Name = L"groupBoxFileEncrypt";
			this->groupBoxFileEncrypt->Padding = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->groupBoxFileEncrypt->Size = System::Drawing::Size(897, 453);
			this->groupBoxFileEncrypt->TabIndex = 22;
			this->groupBoxFileEncrypt->TabStop = false;
			this->groupBoxFileEncrypt->Text = L"Enter password and encrypt file";
			// 
			// labelSelectedStartAddressNumber
			// 
			this->labelSelectedStartAddressNumber->AutoSize = true;
			this->labelSelectedStartAddressNumber->Location = System::Drawing::Point(627, 305);
			this->labelSelectedStartAddressNumber->Name = L"labelSelectedStartAddressNumber";
			this->labelSelectedStartAddressNumber->Size = System::Drawing::Size(23, 18);
			this->labelSelectedStartAddressNumber->TabIndex = 45;
			this->labelSelectedStartAddressNumber->Text = L"---";
			// 
			// labelSelectedStartAddress
			// 
			this->labelSelectedStartAddress->AutoSize = true;
			this->labelSelectedStartAddress->Location = System::Drawing::Point(581, 270);
			this->labelSelectedStartAddress->Name = L"labelSelectedStartAddress";
			this->labelSelectedStartAddress->Size = System::Drawing::Size(159, 18);
			this->labelSelectedStartAddress->TabIndex = 44;
			this->labelSelectedStartAddress->Text = L"Selected start address:";
			// 
			// labelSelectedPacketSizeNumber
			// 
			this->labelSelectedPacketSizeNumber->AutoSize = true;
			this->labelSelectedPacketSizeNumber->Location = System::Drawing::Point(624, 233);
			this->labelSelectedPacketSizeNumber->Name = L"labelSelectedPacketSizeNumber";
			this->labelSelectedPacketSizeNumber->Size = System::Drawing::Size(23, 18);
			this->labelSelectedPacketSizeNumber->TabIndex = 43;
			this->labelSelectedPacketSizeNumber->Text = L"---";
			// 
			// labelSelectedPacketSize
			// 
			this->labelSelectedPacketSize->AutoSize = true;
			this->labelSelectedPacketSize->Location = System::Drawing::Point(581, 198);
			this->labelSelectedPacketSize->Name = L"labelSelectedPacketSize";
			this->labelSelectedPacketSize->Size = System::Drawing::Size(148, 18);
			this->labelSelectedPacketSize->TabIndex = 42;
			this->labelSelectedPacketSize->Text = L"Selected packet size:";
			// 
			// labelResultEncrypt
			// 
			this->labelResultEncrypt->AutoSize = true;
			this->labelResultEncrypt->Location = System::Drawing::Point(579, 127);
			this->labelResultEncrypt->Margin = System::Windows::Forms::Padding(4, 0, 4, 0);
			this->labelResultEncrypt->Name = L"labelResultEncrypt";
			this->labelResultEncrypt->Size = System::Drawing::Size(54, 18);
			this->labelResultEncrypt->TabIndex = 41;
			this->labelResultEncrypt->Text = L"Result:";
			// 
			// groupBoxPassword
			// 
			this->groupBoxPassword->Controls->Add(this->textBoxP15);
			this->groupBoxPassword->Controls->Add(this->labelP0);
			this->groupBoxPassword->Controls->Add(this->labelP2);
			this->groupBoxPassword->Controls->Add(this->labelP15);
			this->groupBoxPassword->Controls->Add(this->textBoxP14);
			this->groupBoxPassword->Controls->Add(this->labelP3);
			this->groupBoxPassword->Controls->Add(this->labelP4);
			this->groupBoxPassword->Controls->Add(this->textBoxP13);
			this->groupBoxPassword->Controls->Add(this->labelP5);
			this->groupBoxPassword->Controls->Add(this->textBoxP12);
			this->groupBoxPassword->Controls->Add(this->labelP6);
			this->groupBoxPassword->Controls->Add(this->labelP1);
			this->groupBoxPassword->Controls->Add(this->textBoxP11);
			this->groupBoxPassword->Controls->Add(this->labelP7);
			this->groupBoxPassword->Controls->Add(this->labelP14);
			this->groupBoxPassword->Controls->Add(this->textBoxP10);
			this->groupBoxPassword->Controls->Add(this->textBoxP0);
			this->groupBoxPassword->Controls->Add(this->textBoxP4);
			this->groupBoxPassword->Controls->Add(this->textBoxP5);
			this->groupBoxPassword->Controls->Add(this->textBoxP9);
			this->groupBoxPassword->Controls->Add(this->textBoxP3);
			this->groupBoxPassword->Controls->Add(this->textBoxP8);
			this->groupBoxPassword->Controls->Add(this->textBoxP6);
			this->groupBoxPassword->Controls->Add(this->textBoxP2);
			this->groupBoxPassword->Controls->Add(this->labelP13);
			this->groupBoxPassword->Controls->Add(this->textBoxP7);
			this->groupBoxPassword->Controls->Add(this->textBoxP1);
			this->groupBoxPassword->Controls->Add(this->labelP8);
			this->groupBoxPassword->Controls->Add(this->labelP9);
			this->groupBoxPassword->Controls->Add(this->labelP12);
			this->groupBoxPassword->Controls->Add(this->labelP10);
			this->groupBoxPassword->Controls->Add(this->labelP11);
			this->groupBoxPassword->Location = System::Drawing::Point(165, 130);
			this->groupBoxPassword->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->groupBoxPassword->Name = L"groupBoxPassword";
			this->groupBoxPassword->Padding = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->groupBoxPassword->Size = System::Drawing::Size(405, 313);
			this->groupBoxPassword->TabIndex = 40;
			this->groupBoxPassword->TabStop = false;
			this->groupBoxPassword->Text = L"Choose bytes for password [0 - 255]";
			// 
			// textBoxP15
			// 
			this->textBoxP15->Location = System::Drawing::Point(281, 276);
			this->textBoxP15->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP15->Name = L"textBoxP15";
			this->textBoxP15->Size = System::Drawing::Size(100, 24);
			this->textBoxP15->TabIndex = 19;
			// 
			// labelP0
			// 
			this->labelP0->AutoSize = true;
			this->labelP0->Location = System::Drawing::Point(11, 26);
			this->labelP0->Name = L"labelP0";
			this->labelP0->Size = System::Drawing::Size(34, 18);
			this->labelP0->TabIndex = 7;
			this->labelP0->Text = L"P[0]";
			this->labelP0->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelP2
			// 
			this->labelP2->AutoSize = true;
			this->labelP2->Location = System::Drawing::Point(11, 97);
			this->labelP2->Name = L"labelP2";
			this->labelP2->Size = System::Drawing::Size(34, 18);
			this->labelP2->TabIndex = 9;
			this->labelP2->Text = L"P[2]";
			this->labelP2->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelP15
			// 
			this->labelP15->AutoSize = true;
			this->labelP15->Location = System::Drawing::Point(223, 276);
			this->labelP15->Name = L"labelP15";
			this->labelP15->Size = System::Drawing::Size(42, 18);
			this->labelP15->TabIndex = 30;
			this->labelP15->Text = L"P[15]";
			this->labelP15->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// textBoxP14
			// 
			this->textBoxP14->Location = System::Drawing::Point(281, 240);
			this->textBoxP14->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP14->Name = L"textBoxP14";
			this->textBoxP14->Size = System::Drawing::Size(100, 24);
			this->textBoxP14->TabIndex = 18;
			// 
			// labelP3
			// 
			this->labelP3->AutoSize = true;
			this->labelP3->Location = System::Drawing::Point(11, 133);
			this->labelP3->Name = L"labelP3";
			this->labelP3->Size = System::Drawing::Size(34, 18);
			this->labelP3->TabIndex = 10;
			this->labelP3->Text = L"P[3]";
			this->labelP3->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelP4
			// 
			this->labelP4->AutoSize = true;
			this->labelP4->Location = System::Drawing::Point(11, 169);
			this->labelP4->Name = L"labelP4";
			this->labelP4->Size = System::Drawing::Size(34, 18);
			this->labelP4->TabIndex = 11;
			this->labelP4->Text = L"P[4]";
			this->labelP4->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// textBoxP13
			// 
			this->textBoxP13->Location = System::Drawing::Point(281, 204);
			this->textBoxP13->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP13->Name = L"textBoxP13";
			this->textBoxP13->Size = System::Drawing::Size(100, 24);
			this->textBoxP13->TabIndex = 17;
			// 
			// labelP5
			// 
			this->labelP5->AutoSize = true;
			this->labelP5->Location = System::Drawing::Point(11, 204);
			this->labelP5->Name = L"labelP5";
			this->labelP5->Size = System::Drawing::Size(34, 18);
			this->labelP5->TabIndex = 12;
			this->labelP5->Text = L"P[5]";
			this->labelP5->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// textBoxP12
			// 
			this->textBoxP12->Location = System::Drawing::Point(281, 169);
			this->textBoxP12->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP12->Name = L"textBoxP12";
			this->textBoxP12->Size = System::Drawing::Size(100, 24);
			this->textBoxP12->TabIndex = 16;
			// 
			// labelP6
			// 
			this->labelP6->AutoSize = true;
			this->labelP6->Location = System::Drawing::Point(11, 240);
			this->labelP6->Name = L"labelP6";
			this->labelP6->Size = System::Drawing::Size(34, 18);
			this->labelP6->TabIndex = 13;
			this->labelP6->Text = L"P[6]";
			this->labelP6->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelP1
			// 
			this->labelP1->AutoSize = true;
			this->labelP1->Location = System::Drawing::Point(11, 62);
			this->labelP1->Name = L"labelP1";
			this->labelP1->Size = System::Drawing::Size(34, 18);
			this->labelP1->TabIndex = 8;
			this->labelP1->Text = L"P[1]";
			this->labelP1->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// textBoxP11
			// 
			this->textBoxP11->Location = System::Drawing::Point(281, 133);
			this->textBoxP11->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP11->Name = L"textBoxP11";
			this->textBoxP11->Size = System::Drawing::Size(100, 24);
			this->textBoxP11->TabIndex = 15;
			// 
			// labelP7
			// 
			this->labelP7->AutoSize = true;
			this->labelP7->Location = System::Drawing::Point(11, 276);
			this->labelP7->Name = L"labelP7";
			this->labelP7->Size = System::Drawing::Size(34, 18);
			this->labelP7->TabIndex = 14;
			this->labelP7->Text = L"P[7]";
			this->labelP7->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelP14
			// 
			this->labelP14->AutoSize = true;
			this->labelP14->Location = System::Drawing::Point(223, 240);
			this->labelP14->Name = L"labelP14";
			this->labelP14->Size = System::Drawing::Size(42, 18);
			this->labelP14->TabIndex = 29;
			this->labelP14->Text = L"P[14]";
			this->labelP14->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// textBoxP10
			// 
			this->textBoxP10->Location = System::Drawing::Point(281, 97);
			this->textBoxP10->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP10->Name = L"textBoxP10";
			this->textBoxP10->Size = System::Drawing::Size(100, 24);
			this->textBoxP10->TabIndex = 14;
			// 
			// textBoxP0
			// 
			this->textBoxP0->Location = System::Drawing::Point(53, 26);
			this->textBoxP0->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP0->Name = L"textBoxP0";
			this->textBoxP0->Size = System::Drawing::Size(100, 24);
			this->textBoxP0->TabIndex = 4;
			// 
			// textBoxP4
			// 
			this->textBoxP4->Location = System::Drawing::Point(53, 169);
			this->textBoxP4->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP4->Name = L"textBoxP4";
			this->textBoxP4->Size = System::Drawing::Size(100, 24);
			this->textBoxP4->TabIndex = 8;
			// 
			// textBoxP5
			// 
			this->textBoxP5->Location = System::Drawing::Point(53, 204);
			this->textBoxP5->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP5->Name = L"textBoxP5";
			this->textBoxP5->Size = System::Drawing::Size(100, 24);
			this->textBoxP5->TabIndex = 9;
			// 
			// textBoxP9
			// 
			this->textBoxP9->Location = System::Drawing::Point(281, 62);
			this->textBoxP9->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP9->Name = L"textBoxP9";
			this->textBoxP9->Size = System::Drawing::Size(100, 24);
			this->textBoxP9->TabIndex = 13;
			// 
			// textBoxP3
			// 
			this->textBoxP3->Location = System::Drawing::Point(53, 133);
			this->textBoxP3->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP3->Name = L"textBoxP3";
			this->textBoxP3->Size = System::Drawing::Size(100, 24);
			this->textBoxP3->TabIndex = 7;
			// 
			// textBoxP8
			// 
			this->textBoxP8->Location = System::Drawing::Point(281, 26);
			this->textBoxP8->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP8->Name = L"textBoxP8";
			this->textBoxP8->Size = System::Drawing::Size(100, 24);
			this->textBoxP8->TabIndex = 12;
			// 
			// textBoxP6
			// 
			this->textBoxP6->Location = System::Drawing::Point(53, 240);
			this->textBoxP6->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP6->Name = L"textBoxP6";
			this->textBoxP6->Size = System::Drawing::Size(100, 24);
			this->textBoxP6->TabIndex = 10;
			// 
			// textBoxP2
			// 
			this->textBoxP2->Location = System::Drawing::Point(53, 97);
			this->textBoxP2->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP2->Name = L"textBoxP2";
			this->textBoxP2->Size = System::Drawing::Size(100, 24);
			this->textBoxP2->TabIndex = 6;
			// 
			// labelP13
			// 
			this->labelP13->AutoSize = true;
			this->labelP13->Location = System::Drawing::Point(223, 204);
			this->labelP13->Name = L"labelP13";
			this->labelP13->Size = System::Drawing::Size(42, 18);
			this->labelP13->TabIndex = 28;
			this->labelP13->Text = L"P[13]";
			this->labelP13->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// textBoxP7
			// 
			this->textBoxP7->Location = System::Drawing::Point(53, 276);
			this->textBoxP7->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP7->Name = L"textBoxP7";
			this->textBoxP7->Size = System::Drawing::Size(100, 24);
			this->textBoxP7->TabIndex = 11;
			// 
			// textBoxP1
			// 
			this->textBoxP1->Location = System::Drawing::Point(53, 62);
			this->textBoxP1->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP1->Name = L"textBoxP1";
			this->textBoxP1->Size = System::Drawing::Size(100, 24);
			this->textBoxP1->TabIndex = 5;
			// 
			// labelP8
			// 
			this->labelP8->AutoSize = true;
			this->labelP8->Location = System::Drawing::Point(223, 26);
			this->labelP8->Name = L"labelP8";
			this->labelP8->Size = System::Drawing::Size(34, 18);
			this->labelP8->TabIndex = 23;
			this->labelP8->Text = L"P[8]";
			this->labelP8->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelP9
			// 
			this->labelP9->AutoSize = true;
			this->labelP9->Location = System::Drawing::Point(223, 62);
			this->labelP9->Name = L"labelP9";
			this->labelP9->Size = System::Drawing::Size(34, 18);
			this->labelP9->TabIndex = 24;
			this->labelP9->Text = L"P[9]";
			this->labelP9->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelP12
			// 
			this->labelP12->AutoSize = true;
			this->labelP12->Location = System::Drawing::Point(223, 169);
			this->labelP12->Name = L"labelP12";
			this->labelP12->Size = System::Drawing::Size(42, 18);
			this->labelP12->TabIndex = 27;
			this->labelP12->Text = L"P[12]";
			this->labelP12->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelP10
			// 
			this->labelP10->AutoSize = true;
			this->labelP10->Location = System::Drawing::Point(223, 97);
			this->labelP10->Name = L"labelP10";
			this->labelP10->Size = System::Drawing::Size(42, 18);
			this->labelP10->TabIndex = 25;
			this->labelP10->Text = L"P[10]";
			this->labelP10->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelP11
			// 
			this->labelP11->AutoSize = true;
			this->labelP11->Location = System::Drawing::Point(223, 133);
			this->labelP11->Name = L"labelP11";
			this->labelP11->Size = System::Drawing::Size(42, 18);
			this->labelP11->TabIndex = 26;
			this->labelP11->Text = L"P[11]";
			this->labelP11->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// textBoxResultEncrypt
			// 
			this->textBoxResultEncrypt->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->textBoxResultEncrypt->Location = System::Drawing::Point(579, 156);
			this->textBoxResultEncrypt->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->textBoxResultEncrypt->Name = L"textBoxResultEncrypt";
			this->textBoxResultEncrypt->ReadOnly = true;
			this->textBoxResultEncrypt->Size = System::Drawing::Size(253, 24);
			this->textBoxResultEncrypt->TabIndex = 39;
			this->textBoxResultEncrypt->TabStop = false;
			// 
			// radioButtonArray
			// 
			this->radioButtonArray->AutoSize = true;
			this->radioButtonArray->Location = System::Drawing::Point(16, 127);
			this->radioButtonArray->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->radioButtonArray->Name = L"radioButtonArray";
			this->radioButtonArray->Size = System::Drawing::Size(128, 22);
			this->radioButtonArray->TabIndex = 0;
			this->radioButtonArray->Text = L"Using an array:";
			this->radioButtonArray->UseVisualStyleBackColor = true;
			// 
			// radioButtonString
			// 
			this->radioButtonString->AutoSize = true;
			this->radioButtonString->Checked = true;
			this->radioButtonString->Location = System::Drawing::Point(16, 89);
			this->radioButtonString->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->radioButtonString->Name = L"radioButtonString";
			this->radioButtonString->Size = System::Drawing::Size(125, 22);
			this->radioButtonString->TabIndex = 5;
			this->radioButtonString->TabStop = true;
			this->radioButtonString->Text = L"Using a String:";
			this->radioButtonString->UseVisualStyleBackColor = true;
			// 
			// labelPassword16
			// 
			this->labelPassword16->AutoSize = true;
			this->labelPassword16->Location = System::Drawing::Point(13, 38);
			this->labelPassword16->Name = L"labelPassword16";
			this->labelPassword16->Size = System::Drawing::Size(150, 18);
			this->labelPassword16->TabIndex = 2;
			this->labelPassword16->Text = L"Password (16 Bytes):";
			// 
			// textBoxPassword
			// 
			this->textBoxPassword->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->textBoxPassword->Location = System::Drawing::Point(165, 89);
			this->textBoxPassword->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxPassword->MaxLength = 16;
			this->textBoxPassword->Name = L"textBoxPassword";
			this->textBoxPassword->PasswordChar = '*';
			this->textBoxPassword->Size = System::Drawing::Size(200, 24);
			this->textBoxPassword->TabIndex = 3;
			this->textBoxPassword->UseSystemPasswordChar = true;
			// 
			// encryptFileBtn
			// 
			this->encryptFileBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->encryptFileBtn->BackColor = System::Drawing::SystemColors::Control;
			this->encryptFileBtn->FlatAppearance->BorderSize = 2;
			this->encryptFileBtn->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->encryptFileBtn->Location = System::Drawing::Point(759, 38);
			this->encryptFileBtn->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->encryptFileBtn->Name = L"encryptFileBtn";
			this->encryptFileBtn->Size = System::Drawing::Size(115, 49);
			this->encryptFileBtn->TabIndex = 20;
			this->encryptFileBtn->TabStop = false;
			this->encryptFileBtn->Text = L"Encrypt file";
			this->encryptFileBtn->UseVisualStyleBackColor = false;
			this->encryptFileBtn->Click += gcnew System::EventHandler(this, &MainForm::encryptFileBtn_Click);
			// 
			// groupBoxSelectFileEncrypt
			// 
			this->groupBoxSelectFileEncrypt->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBoxSelectFileEncrypt->BackColor = System::Drawing::SystemColors::GradientActiveCaption;
			this->groupBoxSelectFileEncrypt->Controls->Add(this->textBoxPacketSize);
			this->groupBoxSelectFileEncrypt->Controls->Add(this->labelPacketSize);
			this->groupBoxSelectFileEncrypt->Controls->Add(this->labelFileStatusEncrypt);
			this->groupBoxSelectFileEncrypt->Controls->Add(this->filePathEncryptText);
			this->groupBoxSelectFileEncrypt->Controls->Add(this->textBoxAddressEncrypt);
			this->groupBoxSelectFileEncrypt->Controls->Add(this->selectFileEncryptBtn);
			this->groupBoxSelectFileEncrypt->Controls->Add(this->labelAddressEncrypt);
			this->groupBoxSelectFileEncrypt->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->groupBoxSelectFileEncrypt->Location = System::Drawing::Point(3, 2);
			this->groupBoxSelectFileEncrypt->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->groupBoxSelectFileEncrypt->Name = L"groupBoxSelectFileEncrypt";
			this->groupBoxSelectFileEncrypt->Padding = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->groupBoxSelectFileEncrypt->Size = System::Drawing::Size(896, 167);
			this->groupBoxSelectFileEncrypt->TabIndex = 21;
			this->groupBoxSelectFileEncrypt->TabStop = false;
			this->groupBoxSelectFileEncrypt->Text = L"Select file";
			// 
			// textBoxPacketSize
			// 
			this->textBoxPacketSize->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->textBoxPacketSize->Location = System::Drawing::Point(200, 129);
			this->textBoxPacketSize->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxPacketSize->Name = L"textBoxPacketSize";
			this->textBoxPacketSize->Size = System::Drawing::Size(132, 24);
			this->textBoxPacketSize->TabIndex = 1;
			// 
			// labelPacketSize
			// 
			this->labelPacketSize->AutoSize = true;
			this->labelPacketSize->Location = System::Drawing::Point(16, 132);
			this->labelPacketSize->Name = L"labelPacketSize";
			this->labelPacketSize->Size = System::Drawing::Size(164, 18);
			this->labelPacketSize->TabIndex = 8;
			this->labelPacketSize->Text = L"Packet size for bin files:";
			// 
			// labelFileStatusEncrypt
			// 
			this->labelFileStatusEncrypt->AutoSize = true;
			this->labelFileStatusEncrypt->Location = System::Drawing::Point(13, 82);
			this->labelFileStatusEncrypt->Name = L"labelFileStatusEncrypt";
			this->labelFileStatusEncrypt->Size = System::Drawing::Size(109, 18);
			this->labelFileStatusEncrypt->TabIndex = 7;
			this->labelFileStatusEncrypt->Text = L"No file selected";
			// 
			// filePathEncryptText
			// 
			this->filePathEncryptText->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->filePathEncryptText->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
			this->filePathEncryptText->AutoCompleteSource = System::Windows::Forms::AutoCompleteSource::FileSystem;
			this->filePathEncryptText->Location = System::Drawing::Point(17, 42);
			this->filePathEncryptText->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->filePathEncryptText->Name = L"filePathEncryptText";
			this->filePathEncryptText->Size = System::Drawing::Size(685, 24);
			this->filePathEncryptText->TabIndex = 0;
			this->filePathEncryptText->TextChanged += gcnew System::EventHandler(this, &MainForm::filePathEncryptText_TextChanged);
			// 
			// textBoxAddressEncrypt
			// 
			this->textBoxAddressEncrypt->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->textBoxAddressEncrypt->Location = System::Drawing::Point(745, 128);
			this->textBoxAddressEncrypt->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxAddressEncrypt->Name = L"textBoxAddressEncrypt";
			this->textBoxAddressEncrypt->Size = System::Drawing::Size(127, 24);
			this->textBoxAddressEncrypt->TabIndex = 2;
			// 
			// selectFileEncryptBtn
			// 
			this->selectFileEncryptBtn->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->selectFileEncryptBtn->BackColor = System::Drawing::SystemColors::Control;
			this->selectFileEncryptBtn->FlatAppearance->BorderSize = 2;
			this->selectFileEncryptBtn->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->selectFileEncryptBtn->Location = System::Drawing::Point(759, 33);
			this->selectFileEncryptBtn->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->selectFileEncryptBtn->Name = L"selectFileEncryptBtn";
			this->selectFileEncryptBtn->Size = System::Drawing::Size(115, 49);
			this->selectFileEncryptBtn->TabIndex = 1;
			this->selectFileEncryptBtn->TabStop = false;
			this->selectFileEncryptBtn->Text = L"Select file";
			this->selectFileEncryptBtn->UseVisualStyleBackColor = false;
			this->selectFileEncryptBtn->Click += gcnew System::EventHandler(this, &MainForm::selectFileEncryptBtn_Click);
			// 
			// labelAddressEncrypt
			// 
			this->labelAddressEncrypt->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->labelAddressEncrypt->AutoSize = true;
			this->labelAddressEncrypt->Location = System::Drawing::Point(375, 130);
			this->labelAddressEncrypt->Name = L"labelAddressEncrypt";
			this->labelAddressEncrypt->Size = System::Drawing::Size(315, 18);
			this->labelAddressEncrypt->TabIndex = 5;
			this->labelAddressEncrypt->Text = L"Start address for bin file in hexadecimal format:";
			// 
			// fileDialogEncrypt
			// 
			this->fileDialogEncrypt->FileName = L"openFileDialog1";
			this->fileDialogEncrypt->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &MainForm::fileDialogEncrypt_FileOk);
			// 
			// MainForm
			// 
			this->AcceptButton = this->sendBtn;
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::GradientActiveCaption;
			this->ClientSize = System::Drawing::Size(905, 703);
			this->Controls->Add(this->tabControl);
			this->Margin = System::Windows::Forms::Padding(4, 4, 4, 4);
			this->MinimumSize = System::Drawing::Size(921, 739);
			this->Name = L"MainForm";
			this->Text = L"STM32F103 Bootloader - Desktop Application";
			this->groupBoxUART->ResumeLayout(false);
			this->groupBoxUART->PerformLayout();
			this->groupBoxFile->ResumeLayout(false);
			this->groupBoxFile->PerformLayout();
			this->groupBoxSend->ResumeLayout(false);
			this->groupBoxSend->PerformLayout();
			this->tabControl->ResumeLayout(false);
			this->tabPageSend->ResumeLayout(false);
			this->tabPageEncrypt->ResumeLayout(false);
			this->groupBoxFileEncrypt->ResumeLayout(false);
			this->groupBoxFileEncrypt->PerformLayout();
			this->groupBoxPassword->ResumeLayout(false);
			this->groupBoxPassword->PerformLayout();
			this->groupBoxSelectFileEncrypt->ResumeLayout(false);
			this->groupBoxSelectFileEncrypt->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion

		//Class needed for threads, so that the GUI doesn't block, while the Sender object tries to connect and
		//sends files
		ref class ThreadWork {
		private:
			MainForm^ pForm;
			Sender^ pSender;
		public:
			ThreadWork(MainForm^ _form, Sender^ _Sender) {
				pForm = _form;
				pSender = _Sender;
			}
			void Connect() {

				//Set the correct text of the Connect Button and the label according to the results of the connection
				pForm->setConnectStatusSafe(pSender->connect());

			}
			void Send(Object^ obj) {

				auto args = safe_cast<Tuple<String^, bool, String^>^>(obj);

				try
				{
					//Send the selected file to the bootloader

					//The hexadecimal start address in the TextBox "textBoxAddress" doesn't start with "0x",
					//if there is a "0x" at the beginn of the addreess, a "FormatException" will be thrown
					pSender->send_File(args->Item1, args->Item2, UInt32::Parse(args->Item3, System::Globalization::NumberStyles::HexNumber));
				}
				catch (FormatException^)
				{
					try {
						//The hexadecimal start address in the TextBox "textBoxAddress" start with "0x"
						pSender->send_File(args->Item1, args->Item2, Convert::ToUInt32(args->Item3, 16));
					}
					catch (ArgumentOutOfRangeException^) {
						//No start address selected, default start address will be used
						pSender->send_File(args->Item1, args->Item2);
					}

				}

				//Set the correct text of the Send Button and the textbox according to the results of the sending process
				pForm->setResultStatusSafe(pSender->finishedProcess());
			}
		};


		/// <summary>This method sets the properties of the "Connect" button and the "ConnectStatus" label
		/// according to the results of the connection.</summary>
	public: void setConnectStatusSafe(bool status) {

		if (labelStatusUart->InvokeRequired) {
			// Invoke if called from an other thread than the main thread
			auto d = gcnew SafeCallDelegate(this, &MainForm::setConnectStatusSafe);
			labelStatusUart->Invoke(d, gcnew array<Object^>{status});

		}
		else {
			this->connectUartBTN->Text = "Disconnect";
			connectMode = false;
			if (status) {
				this->labelStatusUart->Text = "Connected to STM32F103";
				this->labelStatusUart->ForeColor = System::Drawing::Color::Green;
				f103_connected = true;
				// Display the Bootloader Version and the packet size for bin files
				this->labelBootloaderVersionNumber->ForeColor = System::Drawing::Color::Green;
				this->labelReceivedPacketSizeNumber->ForeColor = System::Drawing::Color::Green;
				this->labelBootloaderVersionNumber->Text = "VER" + System::Convert::ToString(mySender->getBootloaderVersion());
				this->labelReceivedPacketSizeNumber->Text = System::Convert::ToString(mySender->getPacketSize());
			}
			else {
				this->labelStatusUart->Text = "Port open, but no STM32F103 detected";
			}
		}
	}
		  /// <summary>This method sets the properties of the "Send" button and the "Result" textbox
		  /// according to the results of the sending process.
		  /// <para>If there was an error, it gets highlighted in the RichTextBox.</para></summary>
	public: void setResultStatusSafe(bool status) {


		if (textBoxResult->InvokeRequired) {
			auto d = gcnew SafeCallDelegate(this, &MainForm::setResultStatusSafe);
			textBoxResult->Invoke(d, gcnew array<Object^>{status});

		}
		else {

			//Deacvtivate send mode and enable all elements again
			sendMode = false;
			this->sendBtn->Text = "Send file";
			this->connectUartBTN->Enabled = true;
			this->comboBoxRate->Enabled = true;
			this->comboBoxPort->Enabled = true;
			this->refreshPortsBtn->Enabled = true;
			this->selectFileBtn->Enabled = true;
			this->filePathText->Enabled = true;
			this->checkBoxAsync->Enabled = true;
			this->textBoxAddress->Enabled = true;

			String^ fileExtension = this->filePathText->Text->Substring(this->filePathText->Text->LastIndexOf("."));

			cli::array<String^>^ result = mySender->getResult();

			//Check if the sending process was finished
			if (status) {
				//Get the result of the sending process


				//The process finished successfully
				if (result[1] == "FINISHED") {
					String^ durationText = Convert::ToString(mySender->getTime());
					this->textBoxResult->Text = "File sent successfully [" + durationText + "s]";
					//Close the serialport
					this->serialPort->Close();
					if (!this->serialPort->IsOpen) {
						this->connectUartBTN->Text = "Connect";
						this->labelStatusUart->Text = "not connected";
						this->labelStatusUart->ForeColor = System::Drawing::Color::Black;
						f103_connected = false;
						this->labelBootloaderVersionNumber->ForeColor = System::Drawing::Color::Black;
						this->labelReceivedPacketSizeNumber->ForeColor = System::Drawing::Color::Black;
						this->labelBootloaderVersionNumber->Text = "---";
						this->labelReceivedPacketSizeNumber->Text = "---";
					}

				}	//There was an error
				else if (String::Equals(fileExtension, ".hex", StringComparison::OrdinalIgnoreCase)) {
					//The file sent was a HEX file

					//Print the type of the error and the line in which it occurred in the TextBox "TextBoxResult"
					this->textBoxResult->Text = result[1] + " in line " + result[0] + "\n";

					//Open the HEX file and show where the error was
					if (valid_file) {
						//Open the HEX file in the RichTextBox
						this->richTextBoxHex->Text = File::ReadAllText(filePathText->Text);

						//Select the line in which the error occured an change the background color to "PeachPuff"
						int firstcharindex = this->richTextBoxHex->GetFirstCharIndexFromLine(Int32::Parse(result[0]) - 1);
						this->richTextBoxHex->Select(firstcharindex, this->richTextBoxHex->Lines[Int32::Parse(result[0]) - 1]->Length);
						this->richTextBoxHex->SelectionBackColor = Color::PeachPuff;

						//Select the bytes, which caused the error
						if (result[1] == "START_CODE_ERROR") {
							this->richTextBoxHex->Select(firstcharindex, 1);
						}
						else if (result[1] == "ADDRESS_ERROR") {
							this->richTextBoxHex->Select(firstcharindex + 3, 4);
						}
						else if (result[1] == "RECORD_TYPE_ERROR") {
							this->richTextBoxHex->Select(firstcharindex + 7, 2);
						}
						else if (result[1] == "CHECKSUM_ERROR") {
							this->richTextBoxHex->Select(firstcharindex + this->richTextBoxHex->Lines[Int32::Parse(result[0]) - 1]->Length - 2, 2);
						}

						//Change the color of the selected bytes to "Red"
						this->richTextBoxHex->SelectionColor = Color::Red;

						//Scroll to the selected line
						this->richTextBoxHex->ScrollToCaret();

					}
				}
				else if (String::Equals(fileExtension, ".bin", StringComparison::OrdinalIgnoreCase)) {
					//The file sent was a bin file

					//Print the type of the error and the packet in which it 
					//occurred in the TextBox "TextBoxResult"
					this->textBoxResult->Text = result[1] + " in Packet " + result[0] + "\n";

					//Open the bin file in binary mode, read all bytes and close the file again
					IO::BinaryReader^ binReader = gcnew System::IO::BinaryReader(System::IO::File::Open(this->filePathText->Text, System::IO::FileMode::Open));
					cli::array<unsigned char>^ Text = binReader->ReadBytes(binReader->BaseStream->Length);
					binReader->Close();

					//Convert the bytes into hexadeciamal digits
					String^ TextHex = BitConverter::ToString(Text);
					//Delete "-" between two bytes (were added by BitConverter)
					TextHex = TextHex->Replace("-", "");

					//Insert NewLine after every 16 bytes
					int lines = TextHex->Length / 32;
					for (int i = 1; i <= lines; i++) {
						TextHex = TextHex->Insert(i * 32 + (i - 1), "\n");
					}

					//Print the bin file in the RichTextBox
					richTextBoxHex->Text = TextHex;

					//Select the packet in which the error occurred and change its background to
					//"PeachPuff", its color to "Red" and scroll to the selected packet
					int firstcharindex = this->richTextBoxHex->GetFirstCharIndexFromLine((Int32::Parse(result[0]) - 1) * 4);
					this->richTextBoxHex->Select(firstcharindex, (this->richTextBoxHex->Lines[Int32::Parse(result[0]) - 1]->Length + 1) * 4);
					this->richTextBoxHex->SelectionBackColor = Color::PeachPuff;
					this->richTextBoxHex->SelectionColor = Color::Red;
					this->richTextBoxHex->ScrollToCaret();

				}
				else {
					this->textBoxResult->Text = result[1] + " in Block " + result[0] + "\n";
					this->richTextBoxHex->Text = "FILE IS ENCRYPTED";
				}
			}
			else if (!status && result[1] == "FILETYPE") {
				this->textBoxResult->Text = "Filetype not supported by this bootloader version!";
			}
			else {
				this->textBoxResult->Text = "Sending file failed, try again.";
				if (result[1] == "DISCONNECTED") {
					//Close the serialport
					this->serialPort->Close();
					if (!this->serialPort->IsOpen) {
						this->connectUartBTN->Text = "Connect";
						this->labelStatusUart->Text = "not connected";
						this->labelStatusUart->ForeColor = System::Drawing::Color::Black;
						f103_connected = false;
						this->textBoxResult->Text += " [CANCELLED]";
						this->labelBootloaderVersionNumber->ForeColor = System::Drawing::Color::Black;
						this->labelReceivedPacketSizeNumber->ForeColor = System::Drawing::Color::Black;
						this->labelBootloaderVersionNumber->Text = "---";
						this->labelReceivedPacketSizeNumber->Text = "---";
					}
				}
			}
		}
	}

		  /// <summary>This method finds all available COM ports and adds them to the ComboBox.</summary>
	private: void findAndPrintComPorts(void) {

		//Find all available COM ports
		array<Object^>^ ports = serialPort->GetPortNames();
		//Sort all COM ports and add them to the ComboBox
		ports->Sort(ports);
		this->comboBoxPort->Items->AddRange(ports);

	}


		   /// <summary>This method loads the UART settings from the app.config file.</summary>
	private: void loadUARTsettings(void) {

		//Check if the key "rate" is existing
		if (System::Configuration::ConfigurationManager::AppSettings["rate"] != nullptr) {

			//Parse the loaded string into a int value and select the baudrate of the serialport
			this->serialPort->BaudRate = Int32::Parse(System::Configuration::ConfigurationManager::AppSettings["rate"]);

			//If the loaded value exists in the ComboBox, the value will be selected in the ComboBox
			if (this->comboBoxRate->Items->Contains(System::Convert::ToString(this->serialPort->BaudRate))) {
				this->comboBoxRate->SelectedItem = (System::Convert::ToString(this->serialPort->BaudRate));
			}
		}

		//Check if the key "port" is existing
		if (System::Configuration::ConfigurationManager::AppSettings["port"] != nullptr) {

			//Select the Portname for the serialport
			this->serialPort->PortName = System::Configuration::ConfigurationManager::AppSettings["port"];

			//If the loaded value exists in the ComboBox, the value will be selected in the ComboBox
			if (this->comboBoxPort->Items->Contains(this->serialPort->PortName)) {
				this->comboBoxPort->SelectedItem = this->serialPort->PortName;
			}
		}
	}

		   /// <summary>This method saves the UART settings into the app.config file.</summary>
	private: void saveUARTsettings(void) {

		//Open the app.config file
		System::Configuration::Configuration^ config = System::Configuration::ConfigurationManager::OpenExeConfiguration(ConfigurationUserLevel::None);

		//Delete the old value for "rate" and add the new value
		config->AppSettings->Settings->Remove("rate");
		String^ rate = System::Convert::ToString(this->serialPort->BaudRate);
		config->AppSettings->Settings->Add("rate", rate);

		//Delete the old value for "port" and add the new value
		config->AppSettings->Settings->Remove("port");
		config->AppSettings->Settings->Add("port", this->serialPort->PortName);

		//Save and refresh the file
		config->Save(ConfigurationSaveMode::Modified);
		ConfigurationManager::RefreshSection("appSettings");

	}

		   /// <summary>This event gets called, after the "refreshPortsBtn" button was clicked.
		   /// <para>The available COM ports will be refreshed.</para> </summary>
	private: System::Void refreshPortsBtn_Click(System::Object^ sender, System::EventArgs^ e) {

		//Clear all items from the ComboBox, and find all available COM ports and add them to the ComboBox
		this->comboBoxPort->Items->Clear();
		this->findAndPrintComPorts();

	}
		   /// <summary>This event gets called, after the "connectUartBtn" button was clicked.
		   /// <para> It will be tried to connect to a bootloader.</para> </summary>
	private: System::Void connectUartBtn_Click(System::Object^ sender, System::EventArgs^ e) {

		//Abort the connection if the Sender object currently tries to connect
		if (connectMode) {
			connectThread->Abort();
			this->serialPort->Close();
			if (!this->serialPort->IsOpen) {
				//Change the text of the button and the labels
				this->connectUartBTN->Text = "Connect";
				this->labelStatusUart->Text = "not connected";
				this->labelBootloaderVersionNumber->ForeColor = System::Drawing::Color::Black;
				this->labelReceivedPacketSizeNumber->ForeColor = System::Drawing::Color::Black;
				this->labelBootloaderVersionNumber->Text = "---";
				this->labelReceivedPacketSizeNumber->Text = "---";

				this->labelStatusUart->ForeColor = System::Drawing::Color::Black;
				f103_connected = false;
				connectMode = false;
			}
		}
		else {

			this->labelStatusUart->Text = String::Empty;

			//Check if the serialport is already open
			if (!serialPort->IsOpen) {

				//Check if all neccessary UART settings are selected
				if (this->comboBoxPort->Text == String::Empty || this->comboBoxRate->Text == String::Empty)
					this->labelStatusUart->Text = "Please select UART settings";

				else {

					try {
						//Get UART settings from ComboBoxes and open serialport
						this->serialPort->PortName = this->comboBoxPort->Text;
						this->serialPort->BaudRate = Int32::Parse(this->comboBoxRate->Text);
						this->serialPort->Open();

						//Change text of the button and the label, save UART settings
						this->connectUartBTN->Text = "Cancel";
						this->labelStatusUart->Text = "Restart STM32F103 to connect to the Desktop Application";
						this->labelStatusUart->ForeColor = System::Drawing::Color::Red;
						connectMode = true;
						saveUARTsettings();

						//Create a new thread for the connection process, so that the GUI doesn't get blocked
						ThreadWork^ worker = gcnew ThreadWork(this, mySender);
						ThreadStart^ threadDelegate = gcnew ThreadStart(worker, &ThreadWork::Connect);
						connectThread = gcnew Thread(threadDelegate);
						connectThread->Start();

					}
					catch (IOException^) {
						//The port could not be opend
						this->connectUartBTN->Text = "Connect";
						this->labelStatusUart->Text = "not connected";
						this->labelBootloaderVersionNumber->ForeColor = System::Drawing::Color::Black;
						this->labelReceivedPacketSizeNumber->ForeColor = System::Drawing::Color::Black;
						this->labelBootloaderVersionNumber->Text = "---";
						this->labelReceivedPacketSizeNumber->Text = "---";
						this->labelStatusUart->ForeColor = System::Drawing::Color::Black;
						f103_connected = false;
						this->labelStatusUart->Text = "Can't connect to this port";

					}
					catch (UnauthorizedAccessException^) {
						//The port is already used by another application
						this->labelStatusUart->Text = "Port already in use by another application";
						this->labelBootloaderVersionNumber->ForeColor = System::Drawing::Color::Black;
						this->labelReceivedPacketSizeNumber->ForeColor = System::Drawing::Color::Black;
						this->labelBootloaderVersionNumber->Text = "---";
						this->labelReceivedPacketSizeNumber->Text = "---";
					}
				}

			}
			else
			{
				//The serialport has to be closed

				this->serialPort->Close();
				if (!this->serialPort->IsOpen) {
					//Change the text of the button and the labels
					this->connectUartBTN->Text = "Connect";
					this->labelStatusUart->Text = "not connected";
					this->labelBootloaderVersionNumber->ForeColor = System::Drawing::Color::Black;
					this->labelReceivedPacketSizeNumber->ForeColor = System::Drawing::Color::Black;
					this->labelBootloaderVersionNumber->Text = "---";
					this->labelReceivedPacketSizeNumber->Text = "---";
					this->labelStatusUart->ForeColor = System::Drawing::Color::Black;
					f103_connected = false;
				}
			}
		}
	}

		   /// <summary>This event gets called, after the "selectFileBtn" button was clicked.
		   /// <para> A FileDialog will be openned.</para> </summary>
	private: System::Void selectFileBtn_Click(System::Object^ sender, System::EventArgs^ e) {

		//Set the filter for the FileDialog, so that HEX and bin files can be selected
		this->fileDialog->Filter = "HEX file (*.hex)|*.hex|BIN file (*.bin)|*.bin|Encrypted bin file (*.bin.crypt)|*.bin.crypt|Encrypted HEX file (*.hex.crypt)|*.hex.crypt";
		this->fileDialog->FilterIndex = 1;
		this->fileDialog->Title = "Select a file";
		this->fileDialog->RestoreDirectory = true;

		//Open the FileDialog
		if (this->fileDialog->ShowDialog() != System::Windows::Forms::DialogResult::OK && this->filePathText->Text == "") {
			//No file was selected
			valid_file = false;
			this->labelFileStatus->Text = "No file selected";
		}

	}

		   /// <summary>This event gets called, after the user selected a valid file in the FileDialog.
		   /// <para> The path of the selected file will be printed in the corresponding TextBox.</para></summary>
	private: System::Void fileDialog_FileOk(System::Object^ sender, System::ComponentModel::CancelEventArgs^ e) {

		//Print the path of the selected file into the TextBox (filePathText)
		filePathText->Text = this->fileDialog->FileName;

	}

		   /// <summary>This event gets called, after text in the TextBox "filePathText" was changed.
		   /// <para> It will be checked if the text in the TextBox represents a valid file.</para></summary>
	private: System::Void filePathText_TextChanged(System::Object^ sender, System::EventArgs^ e) {

		//If the file was selected with the FileDialog, the color of the label will be green, otherwise black
		if (this->filePathText->Text == this->fileDialog->FileName) {
			this->filePathText->ForeColor = System::Drawing::Color::Green;
			this->labelFileStatus->Text = "File OK";
			this->labelFileStatus->ForeColor = System::Drawing::Color::Black;
			valid_file = true;
		}
		else
			this->filePathText->ForeColor = System::Drawing::Color::Black;

		try {

			//Get the file extension of the file selected in the TextBox
			String^ fileExtension = this->filePathText->Text->Substring(this->filePathText->Text->LastIndexOf("."));

			//The file is not a supported file
			if (!String::Equals(fileExtension, ".hex", StringComparison::OrdinalIgnoreCase) && !String::Equals(fileExtension, ".bin", StringComparison::OrdinalIgnoreCase) && !String::Equals(fileExtension, ".crypt", StringComparison::OrdinalIgnoreCase)) {
				this->labelFileStatus->Text = "Not a supported file selected";
				valid_file = false;
				this->labelFileStatus->ForeColor = System::Drawing::Color::Red;
			}
			else {
				//The file is a HEX or bin file -> check if the file exists
				if (!File::Exists(this->filePathText->Text)) {
					this->labelFileStatus->Text = "File does not exist";
					valid_file = false;
					this->labelFileStatus->ForeColor = System::Drawing::Color::Red;
				}
				else {
					//The file is a crypt file-> Check if it is a bin.crypt file
					if (String::Equals(fileExtension, ".crypt", StringComparison::OrdinalIgnoreCase)) {
						String^ fileExtension2 = this->filePathText->Text->Substring(this->filePathText->Text->Length - 10, 4);
						//Not a supported crypt file selected
						if (!String::Equals(fileExtension2, ".bin", StringComparison::OrdinalIgnoreCase) && !String::Equals(fileExtension2, ".hex", StringComparison::OrdinalIgnoreCase)) {
							this->labelFileStatus->Text = "Not a supported file selected";
							valid_file = false;
							this->labelFileStatus->ForeColor = System::Drawing::Color::Red;
						}
					}
					else {
						this->labelFileStatus->Text = "File OK";
						valid_file = true;
						this->labelFileStatus->ForeColor = System::Drawing::Color::Black;
					}
				}
			}
		}
		catch (ArgumentOutOfRangeException^) {

			//File extension coult not be determined
			this->labelFileStatus->ForeColor = System::Drawing::Color::Black;
			valid_file = false;

			if (String::IsNullOrEmpty(filePathText->Text))
				this->labelFileStatus->Text = "No file selected";
			else
				this->labelFileStatus->Text = "No file extension";

		}
	}

		   /// <summary>This event gets called, after the "OpenFileBtn" button was clicked.
		   /// <para> The selected file will be openned in the RichTextBox.</para></summary>
	private: System::Void OpenFileBtn_Click(System::Object^ sender, System::EventArgs^ e) {

		//Check if a valid file was selected
		if (valid_file) {

			//Get the file extension of the selected file
			String^ fileExtension = this->filePathText->Text->Substring(this->filePathText->Text->LastIndexOf("."));

			//A bin file was selected
			if (String::Equals(fileExtension, ".bin", StringComparison::OrdinalIgnoreCase)) {

				//Open the bin file in binary mode, read all bytes and close the file again
				IO::BinaryReader^ binReader = gcnew System::IO::BinaryReader(System::IO::File::Open(this->filePathText->Text, System::IO::FileMode::Open));
				cli::array<unsigned char>^ Text = binReader->ReadBytes(binReader->BaseStream->Length);
				binReader->Close();

				//Convert the bytes into hexadeciamal digits
				String^ TextHex = BitConverter::ToString(Text);
				//Delete "-" between two bytes (were added by BitConverter)
				TextHex = TextHex->Replace("-", "");

				//Insert NewLine after every 16 bytes
				int lines = TextHex->Length / 32;
				for (int i = 1; i <= lines; i++) {
					TextHex = TextHex->Insert(i * 32 + (i - 1), "\n");
				}

				//Print the bin file in the RichTextBox
				this->richTextBoxHex->Text = TextHex;

			}
			else if (String::Equals(fileExtension, ".hex", StringComparison::OrdinalIgnoreCase)) {
				//The file is HEX file -> print the content to the RichTextBox
				this->richTextBoxHex->Text = File::ReadAllText(filePathText->Text);
			}
			else {
				this->richTextBoxHex->Text = "FILE IS ENCRYPTED AND BASE64-ENCODED\n\n";
				this->richTextBoxHex->Text += File::ReadAllText(filePathText->Text);
			}
		}
		else {
			this->richTextBoxHex->Text = "Not a valid file selected";
		}
	}

		   /// <summary>This event gets called, after the "sendBtn" button was clicked.
		   /// <para> The selected file will be sent to the bootlaoder.</para></summary>
	private: System::Void sendBtn_Click(System::Object^ sender, System::EventArgs^ e) {

		//Check if the sending process has to be cancelled
		if (sendMode) {
			//Sleep is necessary so that the ObjectDisposedException doesn't get thrown when trying cancell
			//the sending of a bin file.
			Sleep(2000);
			if (sendThread->ThreadState == Threading::ThreadState::WaitSleepJoin || sendThread->ThreadState == Threading::ThreadState::Running) {
				//Abort the sending thread
				sendThread->Abort();
				//Enable all elements again
				this->sendBtn->Text = "Send file";
				this->connectUartBTN->Enabled = true;
				this->comboBoxRate->Enabled = true;
				this->comboBoxPort->Enabled = true;
				this->refreshPortsBtn->Enabled = true;
				this->selectFileBtn->Enabled = true;
				this->filePathText->Enabled = true;
				this->checkBoxAsync->Enabled = true;
				this->textBoxAddress->Enabled = true;
				this->textBoxResult->Text = "Sending file failed, try again.";
				//Close the serialport
				this->serialPort->Close();
				if (!this->serialPort->IsOpen) {
					this->connectUartBTN->Text = "Connect";
					this->labelStatusUart->Text = "not connected";
					this->labelStatusUart->ForeColor = System::Drawing::Color::Black;
					f103_connected = false;
					this->textBoxResult->Text += " [CANCELLED]";
					this->labelBootloaderVersionNumber->ForeColor = System::Drawing::Color::Black;
					this->labelReceivedPacketSizeNumber->ForeColor = System::Drawing::Color::Black;
					this->labelBootloaderVersionNumber->Text = "---";
					this->labelReceivedPacketSizeNumber->Text = "---";
				}
				sendMode = false;
			}
		}
		else {
			//Check if the desktop application is connected to a bootloader and a valid file was selected
			if (f103_connected) {
				if (valid_file) {

					//Disable all elements which could interrupt the sending process
					this->sendBtn->Text = "Cancel";
					this->connectUartBTN->Enabled = false;
					this->comboBoxRate->Enabled = false;
					this->comboBoxPort->Enabled = false;
					this->refreshPortsBtn->Enabled = false;
					this->selectFileBtn->Enabled = false;
					this->filePathText->Enabled = false;
					this->checkBoxAsync->Enabled = false;
					this->textBoxAddress->Enabled = false;

					//Get the file extension of the selected file
					String^ fileExtension = this->filePathText->Text->Substring(this->filePathText->Text->LastIndexOf("."));

					//Clear the RichTextBox and indicate at the TextBox "textBoxResult" that the sending process is starting
					this->richTextBoxHex->Text = "";
					this->textBoxResult->Text = "Sending...";


					//Check if the file will be sent synchronous or asynchronous
					bool mode = checkBoxAsync->Checked;
					//Entering sendMode
					sendMode = true;


					//Create a new thread for the sending process, so that the GUI doesn't get blocked
					ThreadWork^ worker = gcnew ThreadWork(this, mySender);
					ParameterizedThreadStart^ threadDelegate = gcnew ParameterizedThreadStart(worker, &ThreadWork::Send);
					sendThread = gcnew Thread(threadDelegate);
					sendThread->Start(Tuple::Create(this->filePathText->Text, mode, textBoxAddress->Text));


				}
				else {
					this->textBoxResult->Text = "Not a valid file selected";
				}
			}
			else {
				this->textBoxResult->Text = "Not connected to a STM32F103 bootloader";
			}
		}
	}

		   /// <summary>This event gets called, after the "selectFileEncryptBtn" button was clicked.
		   /// <para> A FileDialog will be openned.</para> </summary>
	private: System::Void selectFileEncryptBtn_Click(System::Object^ sender, System::EventArgs^ e) {
		//Set the filter for the FileDialog, so that HEX and bin files can be selected
		this->fileDialogEncrypt->Filter = "HEX file (*.hex)|*.hex|BIN file (*.bin)|*.bin";
		this->fileDialogEncrypt->FilterIndex = 1;
		this->fileDialogEncrypt->Title = "Select a file to encrypt";
		this->fileDialogEncrypt->RestoreDirectory = true;

		//Open the FileDialog
		if (this->fileDialogEncrypt->ShowDialog() != System::Windows::Forms::DialogResult::OK && this->filePathEncryptText->Text == "") {
			//No file was selected
			valid_file = false;
			this->labelFileStatusEncrypt->Text = "No file selected";
		}
	}

		   /// <summary>This event gets called, after the user selected a valid file in the FileDialog.
		   /// <para> The path of the selected file will be printed in the corresponding TextBox.</para></summary>
	private: System::Void fileDialogEncrypt_FileOk(System::Object^ sender, System::ComponentModel::CancelEventArgs^ e) {
		this->filePathEncryptText->Text = this->fileDialogEncrypt->FileName;
	}

		   /// <summary>This event gets called, afte the text in the TextBox "filePathEncryptText" was changed.
		   /// <para> It will be checked if the text in the TextBox represents a valid file.</para></summary>
	private: System::Void filePathEncryptText_TextChanged(System::Object^ sender, System::EventArgs^ e) {

		//If the file was selected with the FileDialog, the color of the label will be green, otherwise black
		if (this->filePathEncryptText->Text == this->fileDialogEncrypt->FileName) {
			this->filePathEncryptText->ForeColor = System::Drawing::Color::Green;
			this->labelFileStatusEncrypt->Text = "File OK";
			this->labelFileStatusEncrypt->ForeColor = System::Drawing::Color::Black;
			valid_file_encrypt = true;
		}
		else
			this->filePathEncryptText->ForeColor = System::Drawing::Color::Black;

		try {

			//Get the file extension of the file selected in the TextBox
			String^ fileExtension = this->filePathEncryptText->Text->Substring(this->filePathEncryptText->Text->LastIndexOf("."));

			//The file is not a supported file
			if (!String::Equals(fileExtension, ".hex", StringComparison::OrdinalIgnoreCase) && !String::Equals(fileExtension, ".bin", StringComparison::OrdinalIgnoreCase)) {
				this->labelFileStatusEncrypt->Text = "Not a supported file selected";
				valid_file_encrypt = false;
				this->labelFileStatusEncrypt->ForeColor = System::Drawing::Color::Red;
			}
			else {
				//The file is a HEX or bin file -> check if the file exists
				if (!File::Exists(this->filePathEncryptText->Text)) {
					this->labelFileStatusEncrypt->Text = "File does not exist";
					valid_file_encrypt = false;
					this->labelFileStatusEncrypt->ForeColor = System::Drawing::Color::Red;
				}
				else {
					this->labelFileStatusEncrypt->Text = "File OK";
					valid_file_encrypt = true;
					this->labelFileStatusEncrypt->ForeColor = System::Drawing::Color::Black;
				}
			}
		}

		catch (ArgumentOutOfRangeException^) {

			//File extension coult not be determined
			this->labelFileStatusEncrypt->ForeColor = System::Drawing::Color::Black;
			valid_file_encrypt = false;

			if (String::IsNullOrEmpty(filePathEncryptText->Text))
				this->labelFileStatusEncrypt->Text = "No file selected";
			else
				this->labelFileStatusEncrypt->Text = "No file extension";

		}
	}

		   /// <summary>This event gets called, after the "encryptFileBtn" button was clicked.
		   /// <para> The selected file will be encrypted.</para></summary>
	private: System::Void encryptFileBtn_Click(System::Object^ sender, System::EventArgs^ e) {

		// Check if a valid file was selected
		if (valid_file_encrypt) {
			// Create an array for the password
			cli::array<unsigned char>^ passwordBytes = gcnew cli::array<unsigned char>(16);

			// Check which type of password was selected
			if (this->radioButtonString->Checked == true) {
				// The password was entered as a String
				if (this->textBoxPassword->Text->Length == 16)
					// Save the passwaord in the array if the length is correct
					passwordBytes = System::Text::Encoding::Encoding::UTF8->GetBytes(this->textBoxPassword->Text);
				else {
					// The length is not correct
					this->textBoxResultEncrypt->Text = "Password length incorrect";
					return;
				}
			}
			else if (this->radioButtonArray->Checked == true) {
				// The password was entered as an array of bytes
				try {
					// Check if the entered values are in the valid range and save the values
					for (int i = 0; i < 16; i++) {

						int value = Int32::Parse(this->textBoxPasswordArray[i]->Text);

						if (value < 0 || value > 255) {
							this->textBoxResultEncrypt->Text = "Byte " + Convert::ToString(i) + " is not in the valid range";
							return;
						}
						else {
							passwordBytes[i] = value;
						}
					}


				}
				catch (FormatException^) {
					// At least one textbox is empty
					this->textBoxResultEncrypt->Text = "Not all bytes selected";
					return;
				}
			}
			else {
				// No password selected
				this->textBoxResultEncrypt->Text = "Select password settings";
				return;
			}

			// Get the packet size
			unsigned int packetSize;
			try {
				packetSize = UInt32::Parse(this->textBoxPacketSize->Text);
			}
			catch (FormatException^) {
				packetSize = 64u;
			}

			// Get the start address
			unsigned int start_address;
			try {
				start_address = UInt32::Parse(this->textBoxAddressEncrypt->Text, System::Globalization::NumberStyles::HexNumber);
			}
			catch (FormatException^)
			{
				try {
					//The hexadecimal start address in the TextBox "textBoxAddress" start with "0x"
					start_address = Convert::ToUInt32(this->textBoxAddressEncrypt->Text, 16);
				}
				catch (ArgumentOutOfRangeException^) {
					//No start address selected, default start address will be used
					start_address = 0x08006000;
				}
			}

			// Encrypt the file
			this->mySender->encryptFile(this->filePathEncryptText->Text, passwordBytes, start_address, packetSize);

			this->textBoxResultEncrypt->Text = "Finished";

			String^ fileExtension = this->filePathEncryptText->Text->Substring(this->filePathEncryptText->Text->LastIndexOf("."));

			//Display the selected settings for bin files
			if (String::Equals(fileExtension, ".bin", StringComparison::OrdinalIgnoreCase)) {
				this->labelSelectedStartAddressNumber->Text = "0x" + start_address.ToString("X");
				this->labelSelectedPacketSizeNumber->Text = System::Convert::ToString(packetSize);
				this->labelSelectedStartAddressNumber->ForeColor = System::Drawing::Color::Green;
				this->labelSelectedPacketSizeNumber->ForeColor = System::Drawing::Color::Green;
			}
			else {
				this->labelSelectedStartAddressNumber->Text = "---";
				this->labelSelectedPacketSizeNumber->Text = "---";
				this->labelSelectedStartAddressNumber->ForeColor = System::Drawing::Color::Black;
				this->labelSelectedPacketSizeNumber->ForeColor = System::Drawing::Color::Black;
			}
		}
		else {
			this->textBoxResultEncrypt->Text = "Not a valid file selected.";
		}
	}

		   /// <summary>This event gets called, after a tab in the tabcontroll gets changed.
		   /// <para> The accept button will be changed.</para></summary>
	private: System::Void tabControl_SelectedIndexChanged(System::Object^ sender, System::EventArgs^ e) {
		if (tabControl->SelectedIndex == 0)
			this->AcceptButton = this->sendBtn;
		if (tabControl->SelectedIndex == 1)
			this->AcceptButton = this->encryptFileBtn;
	}
	};
}