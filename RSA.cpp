#include "RSA.h"

using namespace System;
using namespace System::Text;
using namespace System::IO;

// This method encrypts an array with RSA
array<unsigned char>^ F103BootloaderDesktop::RSA::RSAEncrypt(array<unsigned char>^ DataToEncrypt, bool DoOAEPPadding)
{
	try
	{
		// The output array
		cli::array<unsigned char, 1>^ encryptedData;
		// Encryption
		encryptedData = p_RSA->Encrypt(DataToEncrypt, DoOAEPPadding);
		// Return the encrypted array
		return encryptedData;
	}
	//Catch a CryptographicException
	catch (CryptographicException^)
	{
		return nullptr;
	}
}


array<unsigned char, 1>^ F103BootloaderDesktop::RSA::RSADecrypt(array<unsigned char, 1>^ DataToDecrypt, bool DoOAEPPadding)
{
	try
	{
		// The output array
		cli::array<unsigned char, 1>^ decryptedData;
		// Decryption
		decryptedData = p_RSA->Decrypt(DataToDecrypt, DoOAEPPadding);
		// Return the decrypted array
		return decryptedData;
	}
	//Catch a CryptographicException
	catch (CryptographicException^)
	{
		return nullptr;
	}
}