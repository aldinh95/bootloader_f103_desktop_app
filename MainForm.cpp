#include "MainForm.h"

using namespace System;
using namespace System::Windows::Forms;


[STAThread]
void main() {

	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	F103BootloaderDesktop::MainForm form;
	Application::Run(% form);

}