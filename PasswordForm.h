#pragma once

namespace F103BootloaderDesktop {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for PasswordForm
	/// </summary>
	public ref class PasswordForm : public System::Windows::Forms::Form
	{
	public:
		PasswordForm(void)
		{
			InitializeComponent();

			// Add the textboxes for the password into an array
			textBoxPasswordArray[0] = this->textBoxP0;
			textBoxPasswordArray[1] = this->textBoxP1;
			textBoxPasswordArray[2] = this->textBoxP2;
			textBoxPasswordArray[3] = this->textBoxP3;
			textBoxPasswordArray[4] = this->textBoxP4;
			textBoxPasswordArray[5] = this->textBoxP5;
			textBoxPasswordArray[6] = this->textBoxP6;
			textBoxPasswordArray[7] = this->textBoxP7;
			textBoxPasswordArray[8] = this->textBoxP8;
			textBoxPasswordArray[9] = this->textBoxP9;
			textBoxPasswordArray[10] = this->textBoxP10;
			textBoxPasswordArray[11] = this->textBoxP11;
			textBoxPasswordArray[12] = this->textBoxP12;
			textBoxPasswordArray[13] = this->textBoxP13;
			textBoxPasswordArray[14] = this->textBoxP14;
			textBoxPasswordArray[15] = this->textBoxP15;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~PasswordForm()
		{
			if (components)
			{
				delete components;
			}
		}

		// Button for setting the password
	private: System::Windows::Forms::Button^ setPasswordBtn;

		   // Groupbox for the password
	private: System::Windows::Forms::GroupBox^ groupBoxPassword;

		   // Label for the password
	private: System::Windows::Forms::Label^ passwordStatusLabel;
	private: System::Windows::Forms::Label^ labelPassword16;
	private: System::Windows::Forms::Label^ labelP0;
	private: System::Windows::Forms::Label^ labelP1;
	private: System::Windows::Forms::Label^ labelP2;
	private: System::Windows::Forms::Label^ labelP3;
	private: System::Windows::Forms::Label^ labelP4;
	private: System::Windows::Forms::Label^ labelP5;
	private: System::Windows::Forms::Label^ labelP6;
	private: System::Windows::Forms::Label^ labelP7;
	private: System::Windows::Forms::Label^ labelP8;
	private: System::Windows::Forms::Label^ labelP9;
	private: System::Windows::Forms::Label^ labelP10;
	private: System::Windows::Forms::Label^ labelP11;
	private: System::Windows::Forms::Label^ labelP12;
	private: System::Windows::Forms::Label^ labelP13;
	private: System::Windows::Forms::Label^ labelP14;
	private: System::Windows::Forms::Label^ labelP15;

		   // Textboxes for the password
	private: System::Windows::Forms::TextBox^ PasswordTextBox;
	private: System::Windows::Forms::TextBox^ textBoxP0;
	private: System::Windows::Forms::TextBox^ textBoxP1;
	private: System::Windows::Forms::TextBox^ textBoxP2;
	private: System::Windows::Forms::TextBox^ textBoxP3;
	private: System::Windows::Forms::TextBox^ textBoxP4;
	private: System::Windows::Forms::TextBox^ textBoxP5;
	private: System::Windows::Forms::TextBox^ textBoxP6;
	private: System::Windows::Forms::TextBox^ textBoxP7;
	private: System::Windows::Forms::TextBox^ textBoxP8;
	private: System::Windows::Forms::TextBox^ textBoxP9;
	private: System::Windows::Forms::TextBox^ textBoxP10;
	private: System::Windows::Forms::TextBox^ textBoxP11;
	private: System::Windows::Forms::TextBox^ textBoxP12;
	private: System::Windows::Forms::TextBox^ textBoxP13;
	private: System::Windows::Forms::TextBox^ textBoxP14;
	private: System::Windows::Forms::TextBox^ textBoxP15;

		   // Radiobutton to select a string or array as password
	private: System::Windows::Forms::RadioButton^ radioButtonArray;
	private: System::Windows::Forms::RadioButton^ radioButtonString;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container^ components;
		// Array of textboxes, used for the textboxes which contain the bytes of the password for encryption
		cli::array<System::Windows::Forms::TextBox^>^ textBoxPasswordArray = gcnew cli::array<System::Windows::Forms::TextBox^>(16);

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->PasswordTextBox = (gcnew System::Windows::Forms::TextBox());
			this->setPasswordBtn = (gcnew System::Windows::Forms::Button());
			this->passwordStatusLabel = (gcnew System::Windows::Forms::Label());
			this->groupBoxPassword = (gcnew System::Windows::Forms::GroupBox());
			this->textBoxP15 = (gcnew System::Windows::Forms::TextBox());
			this->labelP0 = (gcnew System::Windows::Forms::Label());
			this->labelP2 = (gcnew System::Windows::Forms::Label());
			this->labelP15 = (gcnew System::Windows::Forms::Label());
			this->textBoxP14 = (gcnew System::Windows::Forms::TextBox());
			this->labelP3 = (gcnew System::Windows::Forms::Label());
			this->labelP4 = (gcnew System::Windows::Forms::Label());
			this->textBoxP13 = (gcnew System::Windows::Forms::TextBox());
			this->labelP5 = (gcnew System::Windows::Forms::Label());
			this->textBoxP12 = (gcnew System::Windows::Forms::TextBox());
			this->labelP6 = (gcnew System::Windows::Forms::Label());
			this->labelP1 = (gcnew System::Windows::Forms::Label());
			this->textBoxP11 = (gcnew System::Windows::Forms::TextBox());
			this->labelP7 = (gcnew System::Windows::Forms::Label());
			this->labelP14 = (gcnew System::Windows::Forms::Label());
			this->textBoxP10 = (gcnew System::Windows::Forms::TextBox());
			this->textBoxP0 = (gcnew System::Windows::Forms::TextBox());
			this->textBoxP4 = (gcnew System::Windows::Forms::TextBox());
			this->textBoxP5 = (gcnew System::Windows::Forms::TextBox());
			this->textBoxP9 = (gcnew System::Windows::Forms::TextBox());
			this->textBoxP3 = (gcnew System::Windows::Forms::TextBox());
			this->textBoxP8 = (gcnew System::Windows::Forms::TextBox());
			this->textBoxP6 = (gcnew System::Windows::Forms::TextBox());
			this->textBoxP2 = (gcnew System::Windows::Forms::TextBox());
			this->labelP13 = (gcnew System::Windows::Forms::Label());
			this->textBoxP7 = (gcnew System::Windows::Forms::TextBox());
			this->textBoxP1 = (gcnew System::Windows::Forms::TextBox());
			this->labelP8 = (gcnew System::Windows::Forms::Label());
			this->labelP9 = (gcnew System::Windows::Forms::Label());
			this->labelP12 = (gcnew System::Windows::Forms::Label());
			this->labelP10 = (gcnew System::Windows::Forms::Label());
			this->labelP11 = (gcnew System::Windows::Forms::Label());
			this->radioButtonArray = (gcnew System::Windows::Forms::RadioButton());
			this->radioButtonString = (gcnew System::Windows::Forms::RadioButton());
			this->labelPassword16 = (gcnew System::Windows::Forms::Label());
			this->groupBoxPassword->SuspendLayout();
			this->SuspendLayout();
			// 
			// PasswordTextBox
			// 
			this->PasswordTextBox->Location = System::Drawing::Point(181, 43);
			this->PasswordTextBox->MaxLength = 16;
			this->PasswordTextBox->Name = L"PasswordTextBox";
			this->PasswordTextBox->Size = System::Drawing::Size(338, 22);
			this->PasswordTextBox->TabIndex = 0;
			this->PasswordTextBox->UseSystemPasswordChar = true;
			// 
			// setPasswordBtn
			// 
			this->setPasswordBtn->Location = System::Drawing::Point(619, 29);
			this->setPasswordBtn->Name = L"setPasswordBtn";
			this->setPasswordBtn->Size = System::Drawing::Size(114, 36);
			this->setPasswordBtn->TabIndex = 1;
			this->setPasswordBtn->Text = L"Set Password";
			this->setPasswordBtn->UseVisualStyleBackColor = true;
			this->setPasswordBtn->Click += gcnew System::EventHandler(this, &PasswordForm::setPasswordBtn_Click);
			// 
			// passwordStatusLabel
			// 
			this->passwordStatusLabel->AutoSize = true;
			this->passwordStatusLabel->ForeColor = System::Drawing::Color::Red;
			this->passwordStatusLabel->Location = System::Drawing::Point(531, 74);
			this->passwordStatusLabel->Name = L"passwordStatusLabel";
			this->passwordStatusLabel->Size = System::Drawing::Size(0, 17);
			this->passwordStatusLabel->TabIndex = 2;
			// 
			// groupBoxPassword
			// 
			this->groupBoxPassword->Controls->Add(this->textBoxP15);
			this->groupBoxPassword->Controls->Add(this->labelP0);
			this->groupBoxPassword->Controls->Add(this->labelP2);
			this->groupBoxPassword->Controls->Add(this->labelP15);
			this->groupBoxPassword->Controls->Add(this->textBoxP14);
			this->groupBoxPassword->Controls->Add(this->labelP3);
			this->groupBoxPassword->Controls->Add(this->labelP4);
			this->groupBoxPassword->Controls->Add(this->textBoxP13);
			this->groupBoxPassword->Controls->Add(this->labelP5);
			this->groupBoxPassword->Controls->Add(this->textBoxP12);
			this->groupBoxPassword->Controls->Add(this->labelP6);
			this->groupBoxPassword->Controls->Add(this->labelP1);
			this->groupBoxPassword->Controls->Add(this->textBoxP11);
			this->groupBoxPassword->Controls->Add(this->labelP7);
			this->groupBoxPassword->Controls->Add(this->labelP14);
			this->groupBoxPassword->Controls->Add(this->textBoxP10);
			this->groupBoxPassword->Controls->Add(this->textBoxP0);
			this->groupBoxPassword->Controls->Add(this->textBoxP4);
			this->groupBoxPassword->Controls->Add(this->textBoxP5);
			this->groupBoxPassword->Controls->Add(this->textBoxP9);
			this->groupBoxPassword->Controls->Add(this->textBoxP3);
			this->groupBoxPassword->Controls->Add(this->textBoxP8);
			this->groupBoxPassword->Controls->Add(this->textBoxP6);
			this->groupBoxPassword->Controls->Add(this->textBoxP2);
			this->groupBoxPassword->Controls->Add(this->labelP13);
			this->groupBoxPassword->Controls->Add(this->textBoxP7);
			this->groupBoxPassword->Controls->Add(this->textBoxP1);
			this->groupBoxPassword->Controls->Add(this->labelP8);
			this->groupBoxPassword->Controls->Add(this->labelP9);
			this->groupBoxPassword->Controls->Add(this->labelP12);
			this->groupBoxPassword->Controls->Add(this->labelP10);
			this->groupBoxPassword->Controls->Add(this->labelP11);
			this->groupBoxPassword->Location = System::Drawing::Point(181, 95);
			this->groupBoxPassword->Margin = System::Windows::Forms::Padding(4);
			this->groupBoxPassword->Name = L"groupBoxPassword";
			this->groupBoxPassword->Padding = System::Windows::Forms::Padding(4);
			this->groupBoxPassword->Size = System::Drawing::Size(405, 313);
			this->groupBoxPassword->TabIndex = 41;
			this->groupBoxPassword->TabStop = false;
			this->groupBoxPassword->Text = L"Choose bytes for password [0 - 255]";
			// 
			// textBoxP15
			// 
			this->textBoxP15->Location = System::Drawing::Point(281, 276);
			this->textBoxP15->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP15->Name = L"textBoxP15";
			this->textBoxP15->Size = System::Drawing::Size(100, 22);
			this->textBoxP15->TabIndex = 38;
			// 
			// labelP0
			// 
			this->labelP0->AutoSize = true;
			this->labelP0->Location = System::Drawing::Point(11, 26);
			this->labelP0->Name = L"labelP0";
			this->labelP0->Size = System::Drawing::Size(33, 17);
			this->labelP0->TabIndex = 7;
			this->labelP0->Text = L"P[0]";
			this->labelP0->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelP2
			// 
			this->labelP2->AutoSize = true;
			this->labelP2->Location = System::Drawing::Point(11, 97);
			this->labelP2->Name = L"labelP2";
			this->labelP2->Size = System::Drawing::Size(33, 17);
			this->labelP2->TabIndex = 9;
			this->labelP2->Text = L"P[2]";
			this->labelP2->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelP15
			// 
			this->labelP15->AutoSize = true;
			this->labelP15->Location = System::Drawing::Point(223, 276);
			this->labelP15->Name = L"labelP15";
			this->labelP15->Size = System::Drawing::Size(41, 17);
			this->labelP15->TabIndex = 30;
			this->labelP15->Text = L"P[15]";
			this->labelP15->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// textBoxP14
			// 
			this->textBoxP14->Location = System::Drawing::Point(281, 240);
			this->textBoxP14->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP14->Name = L"textBoxP14";
			this->textBoxP14->Size = System::Drawing::Size(100, 22);
			this->textBoxP14->TabIndex = 37;
			// 
			// labelP3
			// 
			this->labelP3->AutoSize = true;
			this->labelP3->Location = System::Drawing::Point(11, 133);
			this->labelP3->Name = L"labelP3";
			this->labelP3->Size = System::Drawing::Size(33, 17);
			this->labelP3->TabIndex = 10;
			this->labelP3->Text = L"P[3]";
			this->labelP3->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelP4
			// 
			this->labelP4->AutoSize = true;
			this->labelP4->Location = System::Drawing::Point(11, 169);
			this->labelP4->Name = L"labelP4";
			this->labelP4->Size = System::Drawing::Size(33, 17);
			this->labelP4->TabIndex = 11;
			this->labelP4->Text = L"P[4]";
			this->labelP4->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// textBoxP13
			// 
			this->textBoxP13->Location = System::Drawing::Point(281, 204);
			this->textBoxP13->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP13->Name = L"textBoxP13";
			this->textBoxP13->Size = System::Drawing::Size(100, 22);
			this->textBoxP13->TabIndex = 36;
			// 
			// labelP5
			// 
			this->labelP5->AutoSize = true;
			this->labelP5->Location = System::Drawing::Point(11, 204);
			this->labelP5->Name = L"labelP5";
			this->labelP5->Size = System::Drawing::Size(33, 17);
			this->labelP5->TabIndex = 12;
			this->labelP5->Text = L"P[5]";
			this->labelP5->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// textBoxP12
			// 
			this->textBoxP12->Location = System::Drawing::Point(281, 169);
			this->textBoxP12->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP12->Name = L"textBoxP12";
			this->textBoxP12->Size = System::Drawing::Size(100, 22);
			this->textBoxP12->TabIndex = 35;
			// 
			// labelP6
			// 
			this->labelP6->AutoSize = true;
			this->labelP6->Location = System::Drawing::Point(11, 240);
			this->labelP6->Name = L"labelP6";
			this->labelP6->Size = System::Drawing::Size(33, 17);
			this->labelP6->TabIndex = 13;
			this->labelP6->Text = L"P[6]";
			this->labelP6->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelP1
			// 
			this->labelP1->AutoSize = true;
			this->labelP1->Location = System::Drawing::Point(11, 62);
			this->labelP1->Name = L"labelP1";
			this->labelP1->Size = System::Drawing::Size(33, 17);
			this->labelP1->TabIndex = 8;
			this->labelP1->Text = L"P[1]";
			this->labelP1->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// textBoxP11
			// 
			this->textBoxP11->Location = System::Drawing::Point(281, 133);
			this->textBoxP11->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP11->Name = L"textBoxP11";
			this->textBoxP11->Size = System::Drawing::Size(100, 22);
			this->textBoxP11->TabIndex = 34;
			// 
			// labelP7
			// 
			this->labelP7->AutoSize = true;
			this->labelP7->Location = System::Drawing::Point(11, 276);
			this->labelP7->Name = L"labelP7";
			this->labelP7->Size = System::Drawing::Size(33, 17);
			this->labelP7->TabIndex = 14;
			this->labelP7->Text = L"P[7]";
			this->labelP7->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelP14
			// 
			this->labelP14->AutoSize = true;
			this->labelP14->Location = System::Drawing::Point(223, 240);
			this->labelP14->Name = L"labelP14";
			this->labelP14->Size = System::Drawing::Size(41, 17);
			this->labelP14->TabIndex = 29;
			this->labelP14->Text = L"P[14]";
			this->labelP14->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// textBoxP10
			// 
			this->textBoxP10->Location = System::Drawing::Point(281, 97);
			this->textBoxP10->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP10->Name = L"textBoxP10";
			this->textBoxP10->Size = System::Drawing::Size(100, 22);
			this->textBoxP10->TabIndex = 33;
			// 
			// textBoxP0
			// 
			this->textBoxP0->Location = System::Drawing::Point(53, 26);
			this->textBoxP0->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP0->Name = L"textBoxP0";
			this->textBoxP0->Size = System::Drawing::Size(100, 22);
			this->textBoxP0->TabIndex = 15;
			// 
			// textBoxP4
			// 
			this->textBoxP4->Location = System::Drawing::Point(53, 169);
			this->textBoxP4->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP4->Name = L"textBoxP4";
			this->textBoxP4->Size = System::Drawing::Size(100, 22);
			this->textBoxP4->TabIndex = 19;
			// 
			// textBoxP5
			// 
			this->textBoxP5->Location = System::Drawing::Point(53, 204);
			this->textBoxP5->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP5->Name = L"textBoxP5";
			this->textBoxP5->Size = System::Drawing::Size(100, 22);
			this->textBoxP5->TabIndex = 20;
			// 
			// textBoxP9
			// 
			this->textBoxP9->Location = System::Drawing::Point(281, 62);
			this->textBoxP9->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP9->Name = L"textBoxP9";
			this->textBoxP9->Size = System::Drawing::Size(100, 22);
			this->textBoxP9->TabIndex = 32;
			// 
			// textBoxP3
			// 
			this->textBoxP3->Location = System::Drawing::Point(53, 133);
			this->textBoxP3->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP3->Name = L"textBoxP3";
			this->textBoxP3->Size = System::Drawing::Size(100, 22);
			this->textBoxP3->TabIndex = 18;
			// 
			// textBoxP8
			// 
			this->textBoxP8->Location = System::Drawing::Point(281, 26);
			this->textBoxP8->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP8->Name = L"textBoxP8";
			this->textBoxP8->Size = System::Drawing::Size(100, 22);
			this->textBoxP8->TabIndex = 31;
			// 
			// textBoxP6
			// 
			this->textBoxP6->Location = System::Drawing::Point(53, 240);
			this->textBoxP6->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP6->Name = L"textBoxP6";
			this->textBoxP6->Size = System::Drawing::Size(100, 22);
			this->textBoxP6->TabIndex = 21;
			// 
			// textBoxP2
			// 
			this->textBoxP2->Location = System::Drawing::Point(53, 97);
			this->textBoxP2->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP2->Name = L"textBoxP2";
			this->textBoxP2->Size = System::Drawing::Size(100, 22);
			this->textBoxP2->TabIndex = 17;
			// 
			// labelP13
			// 
			this->labelP13->AutoSize = true;
			this->labelP13->Location = System::Drawing::Point(223, 204);
			this->labelP13->Name = L"labelP13";
			this->labelP13->Size = System::Drawing::Size(41, 17);
			this->labelP13->TabIndex = 28;
			this->labelP13->Text = L"P[13]";
			this->labelP13->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// textBoxP7
			// 
			this->textBoxP7->Location = System::Drawing::Point(53, 276);
			this->textBoxP7->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP7->Name = L"textBoxP7";
			this->textBoxP7->Size = System::Drawing::Size(100, 22);
			this->textBoxP7->TabIndex = 22;
			// 
			// textBoxP1
			// 
			this->textBoxP1->Location = System::Drawing::Point(53, 62);
			this->textBoxP1->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->textBoxP1->Name = L"textBoxP1";
			this->textBoxP1->Size = System::Drawing::Size(100, 22);
			this->textBoxP1->TabIndex = 16;
			// 
			// labelP8
			// 
			this->labelP8->AutoSize = true;
			this->labelP8->Location = System::Drawing::Point(223, 26);
			this->labelP8->Name = L"labelP8";
			this->labelP8->Size = System::Drawing::Size(33, 17);
			this->labelP8->TabIndex = 23;
			this->labelP8->Text = L"P[8]";
			this->labelP8->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelP9
			// 
			this->labelP9->AutoSize = true;
			this->labelP9->Location = System::Drawing::Point(223, 62);
			this->labelP9->Name = L"labelP9";
			this->labelP9->Size = System::Drawing::Size(33, 17);
			this->labelP9->TabIndex = 24;
			this->labelP9->Text = L"P[9]";
			this->labelP9->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelP12
			// 
			this->labelP12->AutoSize = true;
			this->labelP12->Location = System::Drawing::Point(223, 169);
			this->labelP12->Name = L"labelP12";
			this->labelP12->Size = System::Drawing::Size(41, 17);
			this->labelP12->TabIndex = 27;
			this->labelP12->Text = L"P[12]";
			this->labelP12->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelP10
			// 
			this->labelP10->AutoSize = true;
			this->labelP10->Location = System::Drawing::Point(223, 97);
			this->labelP10->Name = L"labelP10";
			this->labelP10->Size = System::Drawing::Size(41, 17);
			this->labelP10->TabIndex = 25;
			this->labelP10->Text = L"P[10]";
			this->labelP10->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// labelP11
			// 
			this->labelP11->AutoSize = true;
			this->labelP11->Location = System::Drawing::Point(223, 133);
			this->labelP11->Name = L"labelP11";
			this->labelP11->Size = System::Drawing::Size(41, 17);
			this->labelP11->TabIndex = 26;
			this->labelP11->Text = L"P[11]";
			this->labelP11->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// radioButtonArray
			// 
			this->radioButtonArray->AutoSize = true;
			this->radioButtonArray->Location = System::Drawing::Point(12, 104);
			this->radioButtonArray->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->radioButtonArray->Name = L"radioButtonArray";
			this->radioButtonArray->Size = System::Drawing::Size(126, 21);
			this->radioButtonArray->TabIndex = 44;
			this->radioButtonArray->TabStop = true;
			this->radioButtonArray->Text = L"Using an array:";
			this->radioButtonArray->UseVisualStyleBackColor = true;
			// 
			// radioButtonString
			// 
			this->radioButtonString->AutoSize = true;
			this->radioButtonString->Checked = true;
			this->radioButtonString->Location = System::Drawing::Point(12, 43);
			this->radioButtonString->Margin = System::Windows::Forms::Padding(3, 2, 3, 2);
			this->radioButtonString->Name = L"radioButtonString";
			this->radioButtonString->Size = System::Drawing::Size(122, 21);
			this->radioButtonString->TabIndex = 43;
			this->radioButtonString->TabStop = true;
			this->radioButtonString->Text = L"Using a String:";
			this->radioButtonString->UseVisualStyleBackColor = true;
			// 
			// labelPassword16
			// 
			this->labelPassword16->AutoSize = true;
			this->labelPassword16->Location = System::Drawing::Point(9, 9);
			this->labelPassword16->Name = L"labelPassword16";
			this->labelPassword16->Size = System::Drawing::Size(142, 17);
			this->labelPassword16->TabIndex = 42;
			this->labelPassword16->Text = L"Password (16 Bytes):";
			// 
			// PasswordForm
			// 
			this->AcceptButton = this->setPasswordBtn;
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(745, 417);
			this->Controls->Add(this->radioButtonArray);
			this->Controls->Add(this->radioButtonString);
			this->Controls->Add(this->labelPassword16);
			this->Controls->Add(this->groupBoxPassword);
			this->Controls->Add(this->passwordStatusLabel);
			this->Controls->Add(this->setPasswordBtn);
			this->Controls->Add(this->PasswordTextBox);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"PasswordForm";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
			this->Text = L"Enter password for the encrypted file";
			this->groupBoxPassword->ResumeLayout(false);
			this->groupBoxPassword->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
	public:
		// Array which contains the entered password
		cli::array<unsigned char>^ _passwordBytes;
#pragma endregion
		// This event gets called when the password for an encrypted file has to be entered
	private: System::Void setPasswordBtn_Click(System::Object^ sender, System::EventArgs^ e) {

		// The array which contains the password
		cli::array<unsigned char>^ passwordBytes = gcnew cli::array<unsigned char>(16);

		// Check which type of password was selected
		if (this->radioButtonString->Checked == true) {
			// The password was entered as a String
			if (this->PasswordTextBox->Text->Length == 16) {
				// Save the password in the array if the length is correct
				passwordBytes = System::Text::Encoding::Encoding::UTF8->GetBytes(this->PasswordTextBox->Text);
				// Save the password under _passwordBytes,
				// so that the password can be called from outside this form.
				_passwordBytes = passwordBytes;
				// Close the form
				this->DialogResult = System::Windows::Forms::DialogResult::OK;
				this->Close();
			}
			else {
				// The length is not correct
				this->passwordStatusLabel->Text = "Password length incorrect";
			}
		}
		else if (this->radioButtonArray->Checked == true) {
			// The password was entered as an array of bytes
			try {
				// Check if the entered values are in the valid range and save the values
				for (int i = 0; i < 16; i++) {

					int value = Int32::Parse(this->textBoxPasswordArray[i]->Text);

					if (value < 0 || value > 255) {
						this->passwordStatusLabel->Text = "Byte " + Convert::ToString(i) + " is not in the valid range";
						return;
					}
					else {
						passwordBytes[i] = value;
					}
				}
				// Save the password under _passwordBytes,
				// so that the password can be called from outside this form.
				_passwordBytes = passwordBytes;
				// Close the form
				this->DialogResult = System::Windows::Forms::DialogResult::OK;
				this->Close();

			}
			catch (FormatException^) {
				// At least one textbox is empty
				this->passwordStatusLabel->Text = "Not all bytes selected";
				return;
			}
		}
		else {
			// No password selected
			this->passwordStatusLabel->Text = "Select password settings";
			return;
		}
	}
	};
}
