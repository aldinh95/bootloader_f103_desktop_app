#include "AES.h"

using namespace System::Security::Cryptography;
using namespace System::IO;

// This method encrypts an array
array<unsigned char>^ F103BootloaderDesktop::AES::AES_Encrypt(array<unsigned char>^ bytesToBeEncrypted, array<unsigned char>^ passwordBytes, array<unsigned char>^ ivBytes) {
	
	// The output array
	array<unsigned char>^ encryptedBytes = nullptr;

	//Create the necessary objects for encryption
	MemoryStream^ ms = gcnew MemoryStream();
	RijndaelManaged^ AES = gcnew RijndaelManaged();

	try {
		try {
			// Configure the AES object (Keysize, Blocksize, Mode, Padding, Key, iv)
			AES->KeySize = 128;
			AES->BlockSize = 128;
			AES->Mode = CipherMode::CBC;
			AES->Padding = System::Security::Cryptography::PaddingMode::Zeros;
			AES->Key = passwordBytes;
			AES->IV = ivBytes;

			// Encryption
			auto cs = gcnew CryptoStream(ms, AES->CreateEncryptor(), CryptoStreamMode::Write);
			try {
				cs->Write(bytesToBeEncrypted, 0, bytesToBeEncrypted->Length);
				cs->Close();
			}
			// Delete cs
			finally {
				if (cs != nullptr) delete cs;
			}
			// Make an array from Memorystream
			encryptedBytes = ms->ToArray();
		}
		// Delete AES
		finally {
			if (AES != nullptr) delete AES;
		}
	}
	// Delete ms
	finally {
		if (ms != nullptr) delete ms;
	}
	// Return the encrypted array
	return encryptedBytes;
}

array<unsigned char>^ F103BootloaderDesktop::AES::AES_Decrypt(array<unsigned char>^ bytesToBeDecrypted, array<unsigned char>^ passwordBytes, array<unsigned char>^ ivBytes) {
	
	// The output array
	array<unsigned char>^ decryptedBytes = nullptr;

	//Create the necessary objects for encryption
	MemoryStream^ ms = gcnew MemoryStream();
	RijndaelManaged^ AES = gcnew RijndaelManaged();

	try {
		try {
			// Configure the AES object (Keysize, Blocksize, Mode, Padding, Key, iv)
			AES->KeySize = 128;
			AES->BlockSize = 128;
			AES->Mode = CipherMode::CBC;
			AES->Padding = System::Security::Cryptography::PaddingMode::Zeros;
			AES->Key = passwordBytes;
			AES->IV = ivBytes;

			// Decryption
			auto cs = gcnew CryptoStream(ms, AES->CreateDecryptor(), CryptoStreamMode::Write);
			try {
				cs->Write(bytesToBeDecrypted, 0, bytesToBeDecrypted->Length);
				cs->Close();
			}
			// Delete cs
			finally {
				if (cs != nullptr) delete cs;
			}
			// Make an array from Memorystream
			decryptedBytes = ms->ToArray();
		}
		// Delete AES
		finally {
			if (AES != nullptr) delete AES;
		}
	}
	// Delete ms
	finally {
		if (ms != nullptr) delete ms;
	}
	// Return the decrypted array
	return decryptedBytes;
}

//Encrypt a String
System::String^ F103BootloaderDesktop::AES::EncryptText(System::String^ input, System::String^ password, System::String^ iv) {
	// Get the bytes of the string
	array<unsigned char>^ bytesToBeEncrypted = System::Text::Encoding::UTF8->GetBytes(input);
	// Get the bytes of the password
	array<unsigned char>^ passwordBytes = System::Text::Encoding::UTF8->GetBytes(password);
	// Get the bytes of the IV
	array<unsigned char>^ ivBytes = System::Text::Encoding::UTF8->GetBytes(iv);
	// Encrypt
	array<unsigned char>^ bytesEncrypted = AES_Encrypt(bytesToBeEncrypted, passwordBytes, ivBytes);
	// Base64-Encode the result
	System::String^ result = System::Convert::ToBase64String(bytesEncrypted);

	return result;
}

//Decrypt String
System::String^ F103BootloaderDesktop::AES::DecryptText(System::String^ input, System::String^ password, System::String^ iv) {
	// Base64-Decode the string and get the bytes
	array<unsigned char>^ bytesToBeDecrypted = System::Convert::FromBase64String(input);
	// Get the bytes of the password
	array<unsigned char>^ passwordBytes = System::Text::Encoding::Encoding::UTF8->GetBytes(password);
	// Get the bytes of the IV
	array<unsigned char>^ ivBytes = System::Text::Encoding::UTF8->GetBytes(iv);
	// Decrypt the data
	array<unsigned char>^ bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes, ivBytes);
	// Create a String from the decrypted data
	System::String^ result = System::Text::Encoding::Encoding::UTF8->GetString(bytesDecrypted);

	return result;
}