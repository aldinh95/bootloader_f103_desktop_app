#ifndef AES_H_
#define AES_H_


#pragma once
namespace F103BootloaderDesktop {
	ref class AES
	{
	public:
		/// <summary>Initializes a new instance of the AES class.</summary>
		AES(){};

		/// <summary>This method encrypts an array with AES.</summary>
		/// <param name="bytesToBeEncrypted">The array which has to be encrypted.</param>
		/// <param name="passwordBytes">An array which contains the AES key.</param>
		/// <param name="ivBytes">An array which contains the IV for AES encryption.</param>
		/// <returns> The encrypted array </returns>
		cli::array<unsigned char, 1>^ AES_Encrypt(cli::array<unsigned char, 1>^ bytesToBeEncrypted, cli::array<unsigned char, 1>^ passwordBytes, cli::array<unsigned char, 1>^ ivBytes);
		
		/// <summary>This method decrypts an array with AES.</summary>
		/// <param name="bytesToBeDecrypted">The array which has to be decrypted.</param>
		/// <param name="passwordBytes">An array which contains the AES key.</param>
		/// <param name="ivBytes">An array which contains the IV for AES decryption.</param>
		/// <returns> The decrypted array </returns>
		cli::array<unsigned char, 1>^ AES_Decrypt(cli::array<unsigned char, 1>^ bytesToBeDecrypted, cli::array<unsigned char, 1>^ passwordBytes, cli::array<unsigned char, 1>^ ivBytes);
		
		/// <summary>This method encrypts a string with AES.</summary>
		/// <param name="input">The string which has to be encrypted.</param>
		/// <param name="password">A string which contains the AES key.</param>
		/// <param name="iv">A string which contains the IV for AES encryption.</param>
		/// <returns> The encrypted string </returns>
		System::String^ EncryptText(System::String^ input, System::String^ password, System::String^ iv);

		/// <summary>This method decrypts a string with AES.</summary>
		/// <param name="input">The string which has to be decrypted.</param>
		/// <param name="password">A string which contains the AES key.</param>
		/// <param name="iv">A string which contains the IV for AES decryption.</param>
		/// <returns> The decrypted string </returns>
		System::String^ DecryptText(System::String^ input, System::String^ password, System::String^ iv);
	};
}

#endif /* AES_H_ */