#ifndef RSA_H_
#define RSA_H_

using namespace System::Security::Cryptography;

#pragma once
namespace F103BootloaderDesktop {

	ref class RSA
	{
	public:
		/// <summary>Initializes a new instance of the RSA class.</summary>
		RSA() {
			// Create a new RSACryptoServiceProvider object
			p_RSA = gcnew RSACryptoServiceProvider();
		};

		/// <summary>This method encrypts an array with RSA.</summary>
		/// <param name="DataToEncrypt">The array which has to be encrypted.</param>
		/// <param name="DoOAEPPadding">Set the padding
		/// <para>true to perform encryption with OAEP Padding,</para> 
		/// <para> otherwise us PKCS#1 v1.5 padding.</para></param>
		/// <returns> The encrypted array </returns>
		cli::array<unsigned char, 1>^ RSAEncrypt(cli::array<unsigned char, 1>^ DataToEncrypt, bool DoOAEPPadding);
		
		/// <summary>This method decrypts an array with RSA.</summary>
		/// <param name="DataToDecrypt">The array which has to be decrypted.</param>
		/// <param name="DoOAEPPadding">Set the padding
		/// <para>true to perform decryption with OAEP Padding,</para> 
		/// <para> otherwise us PKCS#1 v1.5 padding.</para></param>
		/// <returns> The decrypted array </returns>
		cli::array<unsigned char, 1>^ RSADecrypt(cli::array<unsigned char, 1>^ DataToDecrypt, bool DoOAEPPadding);

		// Pointer to a RSACryptoServiceProvider object
		RSACryptoServiceProvider^ p_RSA;
	};
}

#endif /* RSA_H_ */