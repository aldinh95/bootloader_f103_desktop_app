#ifndef SENDER_H_
#define SENDER_H_

#pragma once
#include <Windows.h>
#include "AES.h"
#include "RSA.h"

#define PACKETSIZE 64u							//The packet size for bin files

namespace F103BootloaderDesktop {

	ref class Sender
	{
	public:
		/// <summary>Initializes a new instance of the Sender class.</summary>
		/// <param name="_serialport">A serialport object, for sending the data through UART.</param>
		Sender(System::IO::Ports::SerialPort^ _serialPort);

	protected:
		~Sender() {
		}
		// Enum which contains all the messages needed for flashing the bootloader
		enum class uart_message {
			ACKN = 0x06u,	//Acknowledgement
			CONNECT = 0x80u,	//Connecting
			HEX_FILE = 0x81u,	//Sending HEX file
			BIN_FILE = 0x82u,	//Sending bin file
			CRYPT_FILE = 0x83u,	//Sending an encrypted file
			SYNC = 0x84u,	//Synchronous sending of a file
			ASYNC = 0x85u,	//Asynchronous sending of a file
			REQUEST_DATA = 0x86u,	//Requesting data
			LINE_OK = 0x87u,	//No error in this line
			PACKET_OK = 0x88u,	//No error in this packet
			FINISHED = 0x89u,	//Finished receiving and no errors
			ADDRESS_ERROR = 0x8Au,	//Address related error
			RECORD_TYPE_ERROR = 0x8Bu, 	//Record type related error
			CHECKSUM_ERROR = 0x8Cu, 	//Checksum related error
			FLASH_ERROR = 0x8Du,	//Flash related error
			START_CODE_ERROR = 0x8Eu,	//Start code wrong
			TERMINATOR_ERROR = 0x8Fu,	//Terminator wrong
			MAX_ERROR = 0x90u,	//Max number of errors reached
			GENERIC_ERROR = 0xFFu,	//Generic error
			DISCONNECTED = 0xFEu,		//Just needed to signalize to the GUI that the SMT32F103 was disconnected while sending a file
			FILETYPE = 0xFDu,	//File type not supported by this bootloader
			CANCELLED = 0xFEu	// The AES password for sending encrypted files was not entered
		};

		//Struct with contains information about the result of a sending process and line number
		ref struct flash_result
		{
			uart_message status = uart_message::LINE_OK;
			int line = 0;
		};

		//The serialport for sending the data through UART
		System::IO::Ports::SerialPort^ p_serialPort;

		//Struct the information about the latest sending process (Status and line number)
		flash_result^ result;

		//Indicates whether the sending process was interrupted and not finished properly
		//No information about the success of the sending process
		bool _finishedProcess;

		//The duration of the latest sending process
		double time;

		//AES object for firmware encryption
		AES^ myAES;
		//RSA object for key exchange
		RSA^ myRSA;

		//The version of the connected bootloader( with or without encryotion, with or without RSA key exchange)
		unsigned char bootloaderVersion;
		//The default start address for bin files, depends on the bootloader version
		unsigned int start_address;
		//The received packet size
		unsigned int received_packet_size;

	public:

		/// <summary>This method connects the Desktop Application to the STM32F103 Bootloader.</summary>
		/// <returns>true, if the connection process was successful, otherwise false.</returns>
		bool connect();

		/// <summary>This method sends a file to the STM32F103 Bootloader.</summary>
		/// <param name="path">A string witch contains the path to the file.</param>
		/// <param name="async">Indicates if the sending process will be asynchronous.</param>
		void send_File(System::String^ path, bool async);

		/// <summary>This method sends a file to the STM32F103 Bootloader.</summary>
		/// <param name="path">A string witch contains the path to the file.</param>
		/// <param name="async">Indicates if the sending process will be asynchronous.</param>
		/// <param name="p_address">The start address for the bin file.</param>
		void send_File(System::String^ path, bool async, unsigned int p_address);

		/// <summary> Get the result of the last sending process.</summary>
		/// <returns> A string, that contains the information about the result of the last sending process. 
		/// <para>The first element is the number of successfully sent lines.</para> 
		/// <para>The second element is the status of the sending process.</para></returns>
		cli::array<System::String^>^ getResult();

		/// <summary> Indicates if the latest sending process was finished.</summary>
		/// <returns> true, if the sending process finished properly, 
		/// regardless of whether it was successful or not, otherwise false</returns>
		bool finishedProcess();


		/// <summary> Get the duration of the latest sending process.</summary>
		/// <returns> The duration of the latest sending process. </returns>
		double getTime();

		/// <summary> Get the Version number of the connected bootlaoder.</summary>
		/// <returns> The Version of the connected bootloader. </returns>
		unsigned char getBootloaderVersion();

		/// <summary> Get the packet size for bin files of the connected bootlaoder.</summary>
		/// <returns> The packet size for bin files of the connected bootlaoder. </returns>
		unsigned int getPacketSize();

		/// <summary>This method encrypts a STM32F103 firmware file.</summary>
		/// <param name="path">A string which contains the path to the file.</param>
		/// <param name="passwordBytes">The key for the AES encryption.</param>
		/// <param name="p_address">The start address for bin files.</param>
		void encryptFile(System::String^ path, cli::array<unsigned char>^ passwordBytes, unsigned int p_address, unsigned int packet_Size);

		/// <summary>This method encrypts a STM32F103 firmware file.</summary>
		/// <param name="path">A string which contains the path to the file.</param>
		/// <param name="passwordBytes">The key for the AES encryption.</param>
		void encryptFile(System::String^ path, cli::array<unsigned char>^ passwordBytes, unsigned int packet_Size);

		/// <summary>This method encrypts a string with RSA.</summary>
		/// <param name="path">The string which has to be encrypted.</param>
		/// <returns>The encrypted text as an string </returns>
		System::String^ RSA_Encrypt(System::String^ text);

		/// <summary>This method decrypts a string with RSA.</summary>
		/// <param name="path">The string which has to be decrypted.</param>
		/// <returns>The decrypted text as an string </returns>
		System::String^ RSA_Decrypt(System::String^ text);

		/// <summary>This method returns the parameters of the public key of the RSA object.</summary>
		/// <returns>The parameters of the public key in an array
		/// <para>The first element is the exponent.</para>
		/// <para>The second element is the modulus.</para></returns>
		cli::array<System::String^>^ getPublicKeyString();

		/// <summary>This method is responsible for RSA key exchange.</summary>
		/// <returns> true, if the key exchange was successful, otherwise false </returns>
		bool sendKey();


	private:
		/// <summary>This method sends a HEX file synchronous to the STM32F103 Bootloader.</summary>
		/// <param name="path">A string which contains the path to the file.</param>
		/// <returns> a flash_result object, which gives information about the success. </returns>
		flash_result^ send_Hex_File(System::String^ path);

		/// <summary>This method sends a HEX file asynchronous to the STM32F103 Bootloader.</summary>
		/// <param name="path">A string which contains the path to the file.</param>
		/// <returns> a flash_result object, which gives information about the success. </returns>
		flash_result^ send_Hex_File_async(System::String^ path);

		/// <summary>This method sends a bin file synchronous to the STM32F103 Bootloader.</summary>
		/// <param name="path">A string witch contains the path to the file.</param>
		/// <param name="p_address">The start address for the bin file.</param>
		/// <returns> a flash_result object, which gives information about the success. </returns>
		flash_result^ send_Bin_File(System::String^ path, unsigned int p_address);

		/// <summary>This method sends a bin file asynchronous to the STM32F103 Bootloader.</summary>
		/// <param name="path">A string witch contains the path to the file.</param>
		/// <param name="p_address">The start address for the bin file.</param>
		/// <returns> a flash_result object, which gives information about the success. </returns>
		flash_result^ send_Bin_File_async(System::String^ path, unsigned int p_address);

		/// <summary>This method calculates the CRC32-checksum of the given data for the bin file.</summary>
		/// <param name="data">A array witch contains the data for the calculation.</param>
		/// <param name="num">The number of the current packet of the bin file.</param>
		/// <param name="length">The length of the current packet of the bin file.</param>
		/// <returns> the CRC32-checksum. </returns>
		unsigned int crc32b(cli::array<unsigned char, 1>^ data, unsigned int num, unsigned int length);

		/// <summary>This method adds the CRC32-checksum of a block right after the bin packet into an array.</summary>
		/// <param name="fileArray">The array in which the block will be written.</param>
		/// <param name="dataArray">The array in which the bytes of the bin file are saved.</param>
		/// <param name="num">The number of the current bin packet.</param>
		/// <param name="length">The size of the current packet.< / param>
		/// <param name="seed">The seed value for random data, which will be added to fill 16 bytes.< / param>
		void addBlock(cli::array<unsigned char, 1>^ fileArray, cli::array<unsigned char, 1>^ dataArray, unsigned int num, unsigned int length, int seed, unsigned int packet_Size);

		/// <summary>This method sends an encrypted bin file synchronous to the STM32F103 Bootloader.</summary>
		/// <param name="path">A string witch contains the path to the encrypted file.</param>
		/// <returns> a flash_result object, which gives information about the success. </returns>
		flash_result^ send_Encrypted_Bin_File(System::String^ path);

		/// <summary>This method sends an encrypted bin file asynchronous to the STM32F103 Bootloader.</summary>
		/// <param name="path">A string witch contains the path to the encrypted file.</param>
		/// <returns> a flash_result object, which gives information about the success. </returns>
		flash_result^ send_Encrypted_Bin_File_async(System::String^ path);

		/// <summary>This method sends an encrypted hex file synchronous to the STM32F103 Bootloader.</summary>
		/// <param name="path">A string witch contains the path to the encrypted file.</param>
		/// <returns> a flash_result object, which gives information about the success. </returns>
		flash_result^ send_Encrypted_Hex_File(System::String^ path);

		/// <summary>This method sends an encrypted hex file asynchronous to the STM32F103 Bootloader.</summary>
		/// <param name="path">A string witch contains the path to the encrypted file.</param>
		/// <returns> a flash_result object, which gives information about the success. </returns>
		flash_result^ send_Encrypted_Hex_File_async(System::String^ path);
	};
}

#endif /* SENDER_H_ */