#include "sender.h"
#include <chrono> 
#include "AES.h"
#include "RSA.h"
#include "PasswordForm.h"

using namespace System;
using namespace std;

#pragma region Concstructor, Connect and send File
// Constructor of the Sender class
F103BootloaderDesktop::Sender::Sender(System::IO::Ports::SerialPort^ _serialPort)
{
	p_serialPort = _serialPort;
	_finishedProcess = false;

	bootloaderVersion = 0;
	start_address = 0;
	received_packet_size = 0;

	myAES = gcnew AES;
	myRSA = gcnew RSA;
}


// Method for connecting the Desktop application to the Bootloader
bool F103BootloaderDesktop::Sender::connect()
{
	// Save the old ReadTimout of the serialport an set timeout to 10
	unsigned int oldReadTimeout = p_serialPort->ReadTimeout;
	p_serialPort->ReadTimeout = 10;

	// Send the CONNECT message to the STM32F103 and wait for the answer
	// The answer has to be the "ACKN"-message, else the variable fails will be increased by 1
	// The process will berepeated until the message is received or the maximum number of errors (1000) is reached
	int fails = 0;
	unsigned char receive = 0;
	unsigned char send = (unsigned char)uart_message::CONNECT;

	while (receive != (unsigned char)uart_message::ACKN && fails < 1000) {

		p_serialPort->BaseStream->WriteByte(send);
		try {
			receive = (this->p_serialPort->ReadByte());
		}
		catch (TimeoutException^) {
			fails++;
		}
	}
	// Get the bootloader version
	bootloaderVersion = this->p_serialPort->ReadByte();
	// Get the default start address
	if (bootloaderVersion == 1)
		start_address = 0x08005400u;
	else if (bootloaderVersion == 2)
		start_address = 0x08003400u;
	else if (bootloaderVersion == 3)
		start_address = 0x08002000u;

	// Get the packet size for bin files from the bootloader
	received_packet_size = this->p_serialPort->ReadByte();

	// Restore the old ReadTimeout
	p_serialPort->ReadTimeout = oldReadTimeout;

	//Check if the connection process was successful or not and return the result
	if (fails == 100)
		return false;
	else
		return true;

}

// Method for sending the a file to the bootloader. Will be called when no address is given.
// Calls an overloaded function
void F103BootloaderDesktop::Sender::send_File(System::String^ path, bool async)
{
	send_File(path, async, start_address);
}

//Method for sending a file to the bootloader.
void F103BootloaderDesktop::Sender::send_File(System::String^ path, bool async, unsigned int p_address)
{
	// Get the file extension of the file, which has to be sent
	String^ fileExtension = path->Substring(path->LastIndexOf("."));
	String^ fileExtension2;

	// The flash_result object in which the result will be saved
	flash_result^ status = gcnew flash_result;

	// Save the old ReadTimout of the serialport an set timeout to 100
	unsigned int oldReadTimeout = p_serialPort->ReadTimeout;
	p_serialPort->ReadTimeout = 100;

	// Set the type of the file (HEX or bin) and the sending mode (synchronous or asynchronous)
	unsigned char type;		//The file type
	unsigned char type2;	//The file type, needed for encrypted files
	unsigned char mode;		//The sending mode

	// Get the file type
	if (String::Equals(fileExtension, ".hex", StringComparison::OrdinalIgnoreCase))
		type = (unsigned char)uart_message::HEX_FILE;
	else if (String::Equals(fileExtension, ".bin", StringComparison::OrdinalIgnoreCase))
		type = (unsigned char)uart_message::BIN_FILE;
	else {
		// Get the file type of the encrypted file
		type = (unsigned char)uart_message::CRYPT_FILE;
		fileExtension2 = path->Substring(path->Length - 10, 4);
		if (String::Equals(fileExtension2, ".HEX", StringComparison::OrdinalIgnoreCase))
			type2 = (unsigned char)uart_message::HEX_FILE;
		else
			type2 = (unsigned char)uart_message::BIN_FILE;
	}


	// Get the sending mode
	if (async)
		mode = (unsigned char)uart_message::ASYNC;
	else
		mode = (unsigned char)uart_message::SYNC;

	if (bootloaderVersion == 3 && (String::Equals(fileExtension, ".crypt", StringComparison::OrdinalIgnoreCase))) {
		status->status = uart_message::FILETYPE;
		this->result = status;
		_finishedProcess = false;
		return;
	}


	// Send the file type
	p_serialPort->BaseStream->WriteByte(type);

	// Send the type of the encryted file, if an encrypted has to be sent
	if (String::Equals(fileExtension, ".crypt", StringComparison::OrdinalIgnoreCase))
		p_serialPort->BaseStream->WriteByte(type2);

	// Send the sending mode
	p_serialPort->BaseStream->WriteByte(mode);

	// Wait for the answer of the bootloader
	// The answer has to be the "ACKN"-message, else the variable fails will be increased by 1
	// The process will berepeated until the message is received or the maximum number of errors (10) is reached
	unsigned char receive = 0;
	int fails = 0;
	try {
		while (receive != (unsigned char)uart_message::ACKN && fails < 10) {

			try {
				receive = (this->p_serialPort->ReadByte());
			}
			catch (TimeoutException^) {
				fails++;
			}

		}

		// Check if the correct answer was received
		if (fails == 10) {
			// The answer was not received
			status->status = uart_message::GENERIC_ERROR;	//A generic error occurred
			status->line = 0;								//in line (or packet) 0
			_finishedProcess = false;						//The sending process didn't finish
		}
		else {
			// The right answer was received
			p_serialPort->ReadTimeout = 10000;				//Set the ReadTimeout to 10.000

			// Choose the right method for sending the file according to the file type and sending mode
			if (String::Equals(fileExtension, ".hex", StringComparison::OrdinalIgnoreCase)) {
				if (async)
					status = this->send_Hex_File_async(path);
				else
					status = this->send_Hex_File(path);
			}
			else if (String::Equals(fileExtension, ".bin", StringComparison::OrdinalIgnoreCase)) {
				if (async)
					status = this->send_Bin_File_async(path, p_address);
				else
					status = this->send_Bin_File(path, p_address);
			}
			else if (String::Equals(fileExtension2, ".hex", StringComparison::OrdinalIgnoreCase)) {
				if (async)
					status = this->send_Encrypted_Hex_File_async(path);
				else
					status = this->send_Encrypted_Hex_File(path);
			}
			else {
				if (async)
					status = this->send_Encrypted_Bin_File_async(path);
				else
					status = this->send_Encrypted_Bin_File(path);

			}


		}
		p_serialPort->ReadTimeout = oldReadTimeout;			//Restore the old ReadTimeout
		this->result = status;								//Set the result of the flashing process
	}
	catch (IO::IOException^) {
		status->status = uart_message::DISCONNECTED;
		status->line = 0;
		_finishedProcess = false;						//The sending process didn't finished
	}
}
#pragma endregion

#pragma region Send a hex file
// The method for sending a HEX file to the bootloader (synchronous)
// After every sent line, the Desktop Application waits for the answer of the bootloader, whether the line
// was received and processed successfully. When no error occurred the next line will be sent, otherwise
// the same line will be sent again
F103BootloaderDesktop::Sender::flash_result^ F103BootloaderDesktop::Sender::send_Hex_File(System::String^ path) {

	// Get the time at the beginn of the method
	auto time1 = std::chrono::high_resolution_clock::now();

	// The flash_result object in which the result will be saved
	flash_result^ status = gcnew flash_result;

	// Read the HEX file and save the file line by line into an array
	cli::array<String^>^ lines = IO::File::ReadAllLines(path);

	// Status of the current line
	uart_message line_status = uart_message::LINE_OK;
	// The error type of the last error
	uart_message error_type = uart_message::LINE_OK;

	// The current line number and the number of lines
	int linenum = 0;
	double linelenght = lines->Length;

	try {

		// The sending process will be repeated as long as the status of the current line is LINE_OK
		while (line_status == uart_message::LINE_OK) {

			// Wait for the REQUEST_DATA message from the Bootloader
			// Each line is requested individually by the Bootloader
			if (this->p_serialPort->ReadByte() == (unsigned char)uart_message::REQUEST_DATA) {

				// Get the current line and save in "line"
				String^ line = lines[linenum];
				// Send the line to the Bootloader
				p_serialPort->Write(line);
				p_serialPort->Write("\r\n");				//Send the line terminator

				// Receive the status of the last line sent
				line_status = (uart_message)this->p_serialPort->ReadByte();

				// Check the received status
				if (line_status == uart_message::LINE_OK) {
					// Increase the current line number if no error occurred
					linenum++;
				}
				else if (line_status != uart_message::MAX_ERROR && line_status != uart_message::FINISHED) {
					// An error occurred, but the maximum number of error is not reached yet
					// Save the error type, and set the status to LINE_OK so that the line can be sent again
					error_type = line_status;
					line_status = uart_message::LINE_OK;
				}
			}
		}
	}
	catch (TimeoutException^) {
		status->status = uart_message::GENERIC_ERROR;
		status->line = linenum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}
	catch (IO::IOException^) {
		status->status = uart_message::DISCONNECTED;
		status->line = linenum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}
	catch (InvalidOperationException^) {
		status->status = uart_message::DISCONNECTED;
		status->line = linenum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}

	// Get the time at the end of the method
	auto time2 = std::chrono::high_resolution_clock::now();
	// Calculate the elapsed time
	std::chrono::duration<double> elapsed = time2 - time1;
	// Save the elpased time in the varible "time"
	time = elapsed.count();

	// Save the status of the sending process in the status object and return the status
	if (line_status == uart_message::FINISHED) {
		status->status = uart_message::FINISHED;
		status->line = linelenght;
	}
	else {
		status->status = error_type;
		status->line = linenum + 1;
	}
	_finishedProcess = true;						//The sending process finished
	return status;
}

// The method for sending a HEX file to the bootloader (asynchronous)
// After every sent line, the Desktop Application checks if there is a message form the Bootloader.
// When an error occured the Bootloader sends a message with the error type to the Desktop Appliation.
// Then the Desktop Application jumps back to this line and sends the line again. When all lines are sent,
// the Application waits for the answer of the Bootloader, whether the process finished or an error occurred.
F103BootloaderDesktop::Sender::flash_result^ F103BootloaderDesktop::Sender::send_Hex_File_async(System::String^ path) {

	// Get the time at the beginn of the method
	auto time1 = std::chrono::high_resolution_clock::now();

	// The flash_result object in which the result will be saved
	flash_result^ status = gcnew flash_result;

	// Read the HEX file and save the file line by line into an array
	cli::array<String^>^ lines = IO::File::ReadAllLines(path);

	// Status of the current line
	uart_message line_status = uart_message::LINE_OK;
	// The error type of the last error
	uart_message error_type = uart_message::LINE_OK;

	// The current line number and the number of lines
	int linenum = 0;
	double linelenght = lines->Length;

	// Save the old ReadTimout of the serialport
	unsigned int oldReadTimeout = p_serialPort->ReadTimeout;

	try {
		// Wait for the first request of data from the bootloader
		if (this->p_serialPort->ReadByte() == (unsigned char)uart_message::REQUEST_DATA) {

			// Set the ReadTimeout to 100
			p_serialPort->ReadTimeout = 100;

			// The sending process will be repeated as long as the status is LINE_OK
			while (line_status == uart_message::LINE_OK) {

				// Check if there are lines to send
				if (linenum < linelenght) {
					// Get the current line
					String^ line = lines[linenum];
					// Send the line to the Bootloader
					p_serialPort->Write(line);
					p_serialPort->Write("\r\n");				//Send the line terminator
				}

				// Check if there is something to read from the Bootloader
				if ((p_serialPort->BytesToRead) == 0) {
					// There are no messages
					// Increase the current line number if not all lines have been sent
					if (linenum < linelenght) {
						linenum++;
					}
				}
				else {
					// There is a message from the Bootloader
					// Read the message
					line_status = (uart_message)this->p_serialPort->ReadByte();

					// Not finished -> an error occurred
					if (line_status != uart_message::FINISHED) {

						// Receive the line number in which the error occurred and jump to this line again
						String^ newLine = this->p_serialPort->ReadLine();
						int newLineNum = (Convert::ToInt32(newLine));
						linenum = newLineNum;

						// Check if maximum number of errors reached
						if (line_status != uart_message::MAX_ERROR) {
							// Save the error_type
							error_type = line_status;
							// Reset the line status to LINE_OK, so that the line can be sent again
							line_status = uart_message::LINE_OK;
							// Send the ACKN-message to the Bootloade to synchronize the Bootloader and the
							// Desktop Application again, so that the sending process can be continued
							p_serialPort->BaseStream->WriteByte((unsigned char)uart_message::ACKN);
						}
					}
				}
			}
		}
	}
	catch (TimeoutException^) {
		status->status = uart_message::GENERIC_ERROR;
		status->line = linenum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}
	catch (IO::IOException^) {
		status->status = uart_message::DISCONNECTED;
		status->line = linenum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}
	catch (InvalidOperationException^) {
		status->status = uart_message::DISCONNECTED;
		status->line = linenum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}

	// Get the time at the end of the method
	auto time2 = std::chrono::high_resolution_clock::now();
	// Calculate the elapsed time
	std::chrono::duration<double> elapsed = time2 - time1;
	// Save the elpased time in the varible "time"
	time = elapsed.count();

	//Restor the old ReadTimeout
	p_serialPort->ReadTimeout = oldReadTimeout;

	// Save the status of the sending process in the status object and return the status
	if (line_status == uart_message::FINISHED) {
		status->status = uart_message::FINISHED;
		status->line = linelenght;
	}
	else {
		status->status = error_type;
		status->line = linenum + 1;
	}
	_finishedProcess = true;		//The sending process finished
	return status;
}

// The method for sending an encrypted HEX file to the bootloader (synchronous)
// After every sent block of the encrypted file, the Desktop Application waits for the answer of the bootloader,
// if an error occurred. When no error occurred the next packet will be sent, otherwise the sending process will be aborted
F103BootloaderDesktop::Sender::flash_result^ F103BootloaderDesktop::Sender::send_Encrypted_Hex_File(System::String^ path)
{
	// Get the time at the beginn of the method
	auto time1 = std::chrono::high_resolution_clock::now();

	// The flash_result object in which the result will be saved
	flash_result^ status = gcnew flash_result;
	status->status = uart_message::ACKN;
	status->line = -1;

	// Status of the current packet
	uart_message block_status = uart_message::LINE_OK;
	// The error type of the last error
	uart_message error_type = uart_message::LINE_OK;

	// Save the old ReadTimout of the serialport
	unsigned int oldReadTimeout = p_serialPort->ReadTimeout;

	// Load the file into a String array
	cli::array<String^>^ blockString = IO::File::ReadAllLines(path);

	// Load the String into unsigned char array, base64-decode
	cli::array<unsigned char>^ file = System::Convert::FromBase64String(blockString[0]);

	// Get the IV and the data
	cli::array<unsigned char>^ iv = gcnew cli::array<unsigned char>(16);
	cli::array<unsigned char>^ data = gcnew cli::array<unsigned char>(file->Length - 16);
	cli::array<unsigned char>::Copy(file, 0, iv, 0, 16);
	cli::array<unsigned char>::Copy(file, 16, data, 0, file->Length - 16);

	// Current packet number
	int blocknum = 0;
	// Calculate the number of blocks
	int size = data->Length - 16;
	int blocks = size / 16;



	p_serialPort->ReadTimeout = 10000;
	try {
	// Wait for the first request of data
		if (this->p_serialPort->ReadByte() == (unsigned char)uart_message::REQUEST_DATA) {

			// Get the key of the encrypted firmware and send the key in encrypted form
			if (bootloaderVersion == 1)
				if (!sendKey()) {
					status->status = uart_message::CANCELLED;
					status->line = 1;
					return status;
				}


			// Send the IV
			for (int i = 0; i < 16; i++) {
				p_serialPort->BaseStream->WriteByte(iv[i]);
			}

			// Repeat while not finished or an error occurred
			while (block_status == uart_message::LINE_OK) {

				// There is a message from the bootloader
				if (p_serialPort->BytesToRead != 0) {

					// Read the message
					block_status = (uart_message)this->p_serialPort->ReadByte();

					// Check the received status
					if (block_status == uart_message::LINE_OK) {

						// The bootloader requested data
						if (this->p_serialPort->ReadByte() == (unsigned char)uart_message::REQUEST_DATA) {

							// Send the next block
							p_serialPort->BaseStream->Write(data, 16 * blocknum, 16);

							// Increment the block number
							blocknum++;
						}
					}
					// An error occurred or finished
					else
					{
						error_type = block_status;
						blocknum--;
						break;
					}
				}
			}

		}
	}
	catch (TimeoutException^) {
		status->status = uart_message::GENERIC_ERROR;
		status->line = blocknum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}
	catch (IO::IOException^) {
		status->status = uart_message::DISCONNECTED;
		status->line = blocknum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}
	catch (InvalidOperationException^) {
		status->status = uart_message::DISCONNECTED;
		status->line = blocknum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}
	// Get the time at the end of the method
	auto time2 = std::chrono::high_resolution_clock::now();
	// Calculate the elapsed time
	std::chrono::duration<double> elapsed = time2 - time1;
	// Save the elpased time in the varible "time"
	time = elapsed.count();

	// Restore the old ReadTimeout
	p_serialPort->ReadTimeout = oldReadTimeout;

	// Save the status of the sending process in the status object and return the status
	if (block_status == uart_message::FINISHED) {
		status->status = uart_message::FINISHED;
		status->line = blocks;
	}
	else {
		status->status = error_type;
		status->line = blocknum + 1;
	}
	_finishedProcess = true;						//The sending process finished
	return status;
}

// The method for sending an encrypted HEX file to the bootloader (asynchronous)
// After every sent block of the encrypted file, the Desktop Application checks if the bootloader has sent a message.
// The desktop application send packets till an error occurs or the process finishes.
// If an error occurs the sending process will be aborted.
F103BootloaderDesktop::Sender::flash_result^ F103BootloaderDesktop::Sender::send_Encrypted_Hex_File_async(System::String^ path)
{
	// Get the time at the beginn of the method
	auto time1 = std::chrono::high_resolution_clock::now();

	// The flash_result object in which the result will be saved
	flash_result^ status = gcnew flash_result;
	status->status = uart_message::ACKN;
	status->line = -1;

	// Status of the current packet
	uart_message block_status = uart_message::LINE_OK;
	// The error type of the last error
	uart_message error_type = uart_message::LINE_OK;

	// Save the old ReadTimout of the serialport
	unsigned int oldReadTimeout = p_serialPort->ReadTimeout;

	// Load the file into a String array
	cli::array<String^>^ blockString = IO::File::ReadAllLines(path);

	// Load the String into unsigned char array, base64-decode
	cli::array<unsigned char>^ file = System::Convert::FromBase64String(blockString[0]);

	// Get the IV and the data
	cli::array<unsigned char>^ iv = gcnew cli::array<unsigned char>(16);
	cli::array<unsigned char>^ data = gcnew cli::array<unsigned char>(file->Length - 16);
	cli::array<unsigned char>::Copy(file, 0, iv, 0, 16);
	cli::array<unsigned char>::Copy(file, 16, data, 0, file->Length - 16);

	// Current packet number
	int blocknum = 0;
	// Calculate the number of blocks
	int size = data->Length - 16;
	int blocks = size / 16;

	p_serialPort->ReadTimeout = 10000;

	try {
		// Wait for the first request of data
		if (this->p_serialPort->ReadByte() == (unsigned char)uart_message::REQUEST_DATA) {
			// Generate a random synchronization message for synchronization after an error occurred
			Random^ rnd = gcnew Random();
			cli::array<byte>^ syncMessage = gcnew cli::array<byte>(8);
			rnd->NextBytes(syncMessage);

			// Send the synchronization message
			for (int i = 0; i < 8; i++) {
				p_serialPort->BaseStream->WriteByte(syncMessage[i]);
			}

			// Get the key of the encrypted firmware and send the key in encrypted form
			if (bootloaderVersion == 1)
				if (!sendKey()) {
					status->status = uart_message::CANCELLED;
					status->line = 1;
					return status;
				}

			// Send the IV
			for (int i = 0; i < 16; i++) {
				p_serialPort->BaseStream->WriteByte(iv[i]);
			}

			// Not finished and no error occured
			while (block_status == uart_message::LINE_OK) {

				// Not all blocks sent yet
				if (blocknum <= blocks) {
					p_serialPort->BaseStream->Write(data, 16 * blocknum, 16);
				}

				// Check if the bootloader has sent a message
				if (p_serialPort->BytesToRead == 0) {
					// Increment the block number if no messages
					if (blocknum <= blocks)
						blocknum++;
				}
				// The bootloader has sent a message
				else {
					// Read the message
					block_status = (uart_message)this->p_serialPort->ReadByte();

					// Check if finished
					if (block_status != uart_message::FINISHED) {
						// An error occurred -> Abort the sending process
						// Receive the packet number in which the error occurred and jump to this packet again
						String^ newPacket = this->p_serialPort->ReadLine();
						int newPacketNum = (Convert::ToInt32(newPacket));
						blocknum = newPacketNum;
						error_type = block_status;
						// Send the synchronization-message to the Bootloader to synchronize the Bootloader
						// and the Desktop Application again, so that the sending process can be continued
						// The message can be chosen freely, but it has to be the same message like in the
						// bootloader.
						p_serialPort->BaseStream->Write(syncMessage, 0, 8);
						p_serialPort->Write("\n");
						break;
					}
				}
			}
		}
	}
	catch (TimeoutException^) {
		status->status = uart_message::GENERIC_ERROR;
		status->line = blocknum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}
	catch (IO::IOException^) {
		status->status = uart_message::DISCONNECTED;
		status->line = blocknum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}
	catch (InvalidOperationException^) {
		status->status = uart_message::DISCONNECTED;
		status->line = blocknum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}

	// Get the time at the end of the method
	auto time2 = std::chrono::high_resolution_clock::now();
	// Calculate the elapsed time
	std::chrono::duration<double> elapsed = time2 - time1;
	// Save the elpased time in the varible "time"
	time = elapsed.count();

	// Restore the old ReadTimeout
	p_serialPort->ReadTimeout = oldReadTimeout;

	// Save the status of the sending process in the status object and return the status
	if (block_status == uart_message::FINISHED) {
		status->status = uart_message::FINISHED;
		status->line = blocks;
	}
	else {
		status->status = error_type;
		status->line = blocknum + 1;
	}
	_finishedProcess = true;						//The sending process finished
	return status;
}

#pragma endregion

#pragma region Send a bin file
// The method for sending a bin file to the bootloader (synchronous)
// After every sent packet, the Desktop Application waits for the answer of the bootloader, whether the packet
// was received and processed successfully. When no error occurred the next packet will be sent, otherwise
// the same packet will be sent again
F103BootloaderDesktop::Sender::flash_result^ F103BootloaderDesktop::Sender::send_Bin_File(System::String^ path, unsigned int p_address)
{
	// Get the time at the beginn of the method
	auto time1 = std::chrono::high_resolution_clock::now();

	// The flash_result object in which the result will be saved
	flash_result^ status = gcnew flash_result;
	status->status = uart_message::ACKN;
	status->line = -1;

	// Status of the current packet
	uart_message packet_status = uart_message::PACKET_OK;
	// The error type of the last error
	uart_message error_type = uart_message::PACKET_OK;

	// Save the old ReadTimout of the serialport
	unsigned int oldReadTimeout = p_serialPort->ReadTimeout;

	// Read the bin file, save the file into an array and close the file
	IO::BinaryReader^ binReader = gcnew System::IO::BinaryReader(System::IO::File::Open(path, System::IO::FileMode::Open));
	cli::array<unsigned char>^ Packets = binReader->ReadBytes(binReader->BaseStream->Length);
	binReader->Close();

	// Get the size of the the file and convert it into hexadecimal format
	int size = Packets->Length;
	String^ sizeHex = size.ToString("X");

	// Convert the start address for the bin file into hexadecimal format
	String^ addressHex = p_address.ToString("X");

	// Current packet number
	int packetnum = 0;
	// Calculate the number of packets.
	// One packet has to be added if the last packet has not the full size ( < received_packet_size)
	int packets = size / received_packet_size;
	if (packets * received_packet_size != size)
		packets++;

	try {
		// Wait for the request for the size and start address of the bin file from the Bootloader
		if (this->p_serialPort->ReadByte() == (unsigned char)uart_message::REQUEST_DATA) {
			// Send the size of the file
			p_serialPort->Write(sizeHex);
			p_serialPort->Write("\r");
			// Send the start address of the file
			p_serialPort->Write(addressHex);
			p_serialPort->Write("\r");

			// Set the ReadTimeout to 10000;
			p_serialPort->ReadTimeout = 10000;

			// The sending process will be repeated as long as the status of the current packet is PACKET_OK
			while (packet_status == uart_message::PACKET_OK) {
				// Wait for the REQUEST_DATA message from the Bootloader
				// Each packet is requested individually by the Bootloader
				if (this->p_serialPort->ReadByte() == (unsigned char)uart_message::REQUEST_DATA) {

					// Calculate the size of the current packet (the last packet may be smaller)
					int current_packet_size = (packetnum != packets - 1) ? received_packet_size : size % received_packet_size;
					// Send the packet to the Bootloader
					p_serialPort->BaseStream->Write(Packets, packetnum * received_packet_size, current_packet_size);

					// Calculate the CRC32-checksum for the sent data, transform it to the hexadecimal format
					// and send the checksum to the Bootloader
					unsigned int crc = crc32b(Packets, packetnum, current_packet_size);
					String^ crcHex = crc.ToString("X");
					p_serialPort->Write(crcHex);
					p_serialPort->Write("\r");

					// Receive the status of the last packet sent
					packet_status = (uart_message)this->p_serialPort->ReadByte();

					// Check the received status
					if (packet_status == uart_message::PACKET_OK) {
						// Increase the current packet number if no error occurred
						packetnum++;
					}
					else if (packet_status != uart_message::MAX_ERROR && packet_status != uart_message::FINISHED) {
						// An error occurred, but the maximum number of error is not reached yet
						// Save the error type, and set the status to PACKET_OK so that the packet can be sent again
						error_type = packet_status;
						packet_status = uart_message::PACKET_OK;
					}
				}
			}
		}
	}
	catch (TimeoutException^) {
		status->status = uart_message::GENERIC_ERROR;
		status->line = packetnum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}
	catch (IO::IOException^) {
		status->status = uart_message::DISCONNECTED;
		status->line = packetnum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}
	catch (InvalidOperationException^) {
		status->status = uart_message::DISCONNECTED;
		status->line = packetnum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}

	// Get the time at the end of the method
	auto time2 = std::chrono::high_resolution_clock::now();
	// Calculate the elapsed time
	std::chrono::duration<double> elapsed = time2 - time1;
	// Save the elpased time in the varible "time"
	time = elapsed.count();

	// Restore the old ReadTimeout
	p_serialPort->ReadTimeout = oldReadTimeout;


	// Save the status of the sending process in the status object and return the status
	if (packet_status == uart_message::FINISHED) {
		status->status = uart_message::FINISHED;
		status->line = packets;
	}
	else {
		status->status = error_type;
		status->line = packetnum + 1;
	}
	_finishedProcess = true;						//The sending process finished
	return status;
}

// The method for sending a bin file to the bootloader (asynchronous)
// After every sent packet, the Desktop Application checks if there is a message form the Bootloader.
// When an error occured the Bootloader sends a message with the error type to the Desktop Appliation.
// Then the Desktop Application jumps back to this packet and sends the packet again. When all packets are sent,
// the Application waits for the answer of the Bootloader, whether the process finished or an error occurred.
F103BootloaderDesktop::Sender::flash_result^ F103BootloaderDesktop::Sender::send_Bin_File_async(System::String^ path, unsigned int p_address)
{
	// Get the time at the beginn of the method
	auto time1 = std::chrono::high_resolution_clock::now();

	// The flash_result object in which the result will be saved
	flash_result^ status = gcnew flash_result;
	status->status = uart_message::ACKN;
	status->line = -1;

	// Status of the current packet
	uart_message packet_status = uart_message::PACKET_OK;
	// The error type of the last error
	uart_message error_type = uart_message::PACKET_OK;

	// Save the old ReadTimout of the serialport
	unsigned int oldReadTimeout = p_serialPort->ReadTimeout;

	// Read the bin file, save the file into an array and close the file
	IO::BinaryReader^ binReader = gcnew System::IO::BinaryReader(System::IO::File::Open(path, System::IO::FileMode::Open));
	cli::array<unsigned char>^ Packets = binReader->ReadBytes(binReader->BaseStream->Length);
	binReader->Close();

	// Get the size of the the file and convert it into hexadecimal format
	int size = Packets->Length;
	String^ sizeHex = size.ToString("X");

	// Convert the start address for the bin file into hexadecimal format
	String^ addressHex = p_address.ToString("X");

	// Current packet number
	int packetnum = 0;
	// Calculate the number of packets.
	// One packet has to be added if the last packet has not the full size ( < received_packet_size)
	int packets = size / received_packet_size;
	if (packets * received_packet_size != size)
		packets++;

	try {
		// Wait for the request for the size and start address of the bin file from the Bootloader
		if (this->p_serialPort->ReadByte() == (unsigned char)uart_message::REQUEST_DATA) {
			// Generate a random synchronization message for synchronization after an error occurred
			Random^ rnd = gcnew Random();
			cli::array<byte>^ syncMessage = gcnew cli::array<byte>(8);
			rnd->NextBytes(syncMessage);

			// Send the synchronization message
			for (int i = 0; i < 8; i++) {
				p_serialPort->BaseStream->WriteByte(syncMessage[i]);
			}

			// Send the size of the file
			p_serialPort->Write(sizeHex);
			p_serialPort->Write("\r");
			// Send the start address of the file
			p_serialPort->Write(addressHex);
			p_serialPort->Write("\r");

			// Set the ReadTimeout to 10000;
			p_serialPort->ReadTimeout = 10000;


			// The sending process will be repeated as long as the status of the current packet is PACKET_OK
			while (packet_status == uart_message::PACKET_OK) {
				// Check if there are packets to send
				if (packetnum < packets) {


					// Calculate the size of the current packet (the last packet may be smaller)
					int current_packet_size = (packetnum != packets - 1) ? received_packet_size : size % received_packet_size;
					// Send the packet to the Bootloader
					p_serialPort->BaseStream->Write(Packets, packetnum * received_packet_size, current_packet_size);

					// Calculate the CRC32-checksum for the sent data, transform it to the hexadecimal format
					// and send the checksum to the Bootloader
					unsigned int crc = crc32b(Packets, packetnum, current_packet_size);
					String^ crcHex = crc.ToString("X");
					p_serialPort->Write(crcHex);
					p_serialPort->Write("\r");

				}
				// Check if there is something to read from the Bootloader
				if ((p_serialPort->BytesToRead) == 0) {
					// There are no messages
					// Increase the current line number if not all lines have been sent
					if (packetnum < packets) {
						packetnum++;
					}
				}
				else {
					// There is a message from the Bootloader
					// Read the message
					packet_status = (uart_message)this->p_serialPort->ReadByte();

					// Check if maximum number of errors reached
					if (packet_status != uart_message::FINISHED) {
						// Receive the packet number in which the error occurred and jump to this packet again
						String^ newPacket = this->p_serialPort->ReadLine();
						int newPacketNum = (Convert::ToInt32(newPacket));
						packetnum = newPacketNum;



						// Reset the packet status to PACKET_OK, so that the packet can be sent again
						if (packet_status != uart_message::MAX_ERROR) {
							// Save the error_type
							error_type = packet_status;

							packet_status = uart_message::PACKET_OK;
						}
						// Send the synchronization-message to the Bootloader to synchronize the Bootloader
						// and the Desktop Application again, so that the sending process can be continued
						// The message can be chosen freely, but it has to be the same message like in the
						// bootloader. The message shouldn't be too trivial, otherwise there is a possibility
						// that it could also appear in the bin file. Then the bootloader could read the 
						// message at the wrong time.
						p_serialPort->BaseStream->Write(syncMessage, 0, 8);
						p_serialPort->Write("\n");
					}

				}
			}
		}
	}
	catch (TimeoutException^) {
		status->status = uart_message::GENERIC_ERROR;
		status->line = packetnum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}
	catch (IO::IOException^) {
		status->status = uart_message::DISCONNECTED;
		status->line = packetnum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}
	catch (InvalidOperationException^) {
		status->status = uart_message::DISCONNECTED;
		status->line = packetnum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}

	// Get the time at the end of the method
	auto time2 = std::chrono::high_resolution_clock::now();
	// Calculate the elapsed time
	std::chrono::duration<double> elapsed = time2 - time1;
	// Save the elpased time in the varible "time"
	time = elapsed.count();

	// Restore the old ReadTimeout
	p_serialPort->ReadTimeout = oldReadTimeout;

	// Save the status of the sending process in the status object and return the status
	if (packet_status == uart_message::FINISHED) {
		status->status = uart_message::FINISHED;
		status->line = packets;
	}
	else {
		status->status = error_type;
		status->line = packetnum + 1;
	}
	_finishedProcess = true;						//The sending process finished
	return status;
}
// The method for sending an encrypted bin file to the bootloader (synchronous)
// After every sent packet of the encrypted file, the Desktop Application waits for the answer of the bootloader, 
// whether the packet was received and processed successfully. When no error occurred the next packet will be sent,
// otherwise the same packet will be sent again
F103BootloaderDesktop::Sender::flash_result^ F103BootloaderDesktop::Sender::send_Encrypted_Bin_File(System::String^ path)
{
	// Get the time at the beginn of the method
	auto time1 = std::chrono::high_resolution_clock::now();

	// The flash_result object in which the result will be saved
	flash_result^ status = gcnew flash_result;
	status->status = uart_message::ACKN;
	status->line = -1;

	// Status of the current packet
	uart_message block_status = uart_message::PACKET_OK;
	// The error type of the last error
	uart_message error_type = uart_message::PACKET_OK;

	// Save the old ReadTimout of the serialport
	unsigned int oldReadTimeout = p_serialPort->ReadTimeout;

	// Load the file into a String array
	cli::array<String^>^ blockString = IO::File::ReadAllLines(path);

	// Load the String into unsigned char array, base64-decode
	cli::array<unsigned char>^ file = System::Convert::FromBase64String(blockString[0]);

	// Get the IV and the data
	cli::array<unsigned char>^ iv = gcnew cli::array<unsigned char>(16);
	cli::array<unsigned char>^ data = gcnew cli::array<unsigned char>(file->Length - 16);
	cli::array<unsigned char>::Copy(file, 0, iv, 0, 16);
	cli::array<unsigned char>::Copy(file, 16, data, 0, file->Length - 16);

	// Current packet number
	int blocknum = 0;
	// Calculate the number of blocks
	int size = data->Length - 16;
	int blocks = size / (received_packet_size + 16);

	p_serialPort->ReadTimeout = 10000;

	try {
		// Wait for the first request of data
		if (this->p_serialPort->ReadByte() == (unsigned char)uart_message::REQUEST_DATA) {

			// Get the key of the encrypted firmware and send the key in encrypted form
			if (bootloaderVersion == 1)
				if (!sendKey()) {
					status->status = uart_message::CANCELLED;
					status->line = 1;
					return status;
				}

			// Send IV (not encrypted)
			for (int i = 0; i < 16; i++) {
				p_serialPort->BaseStream->WriteByte(iv[i]);
			}
			// Send size and start address (encrypted)
			for (int i = 0; i < 16; i++) {
				p_serialPort->BaseStream->WriteByte(data[i]);
			}

			// Not finished and not max error reached
			while (block_status == uart_message::PACKET_OK) {
				// Wait for the REQUEST_DATA message from the Bootloader
				// Each packet is requested individually by the Bootloader
				if (this->p_serialPort->ReadByte() == (unsigned char)uart_message::REQUEST_DATA) {

					// Send the a packet to the Bootloader
					p_serialPort->BaseStream->Write(data, 16 + (received_packet_size + 16) * blocknum, received_packet_size);

					// Send the block with the crc32 checksum to the bootloader
					p_serialPort->BaseStream->Write(data, 16 + (received_packet_size + 16) * blocknum + received_packet_size, 16);

					// Receive the status of the last packet sent
					block_status = (uart_message)this->p_serialPort->ReadByte();

					// Check the received status
					if (block_status == uart_message::PACKET_OK) {
						// Increase the current packet number if no error occurred
						blocknum++;
					}
					else if (block_status != uart_message::MAX_ERROR && block_status != uart_message::FINISHED) {
						// An error occurred, but the maximum number of error is not reached yet
						// Save the error type, and set the status to PACKET_OK so that the packet can be sent again
						error_type = block_status;
						block_status = uart_message::PACKET_OK;
					}
				}
			}
		}
	}
	catch (TimeoutException^) {
		status->status = uart_message::GENERIC_ERROR;
		status->line = blocknum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}
	catch (IO::IOException^) {
		status->status = uart_message::DISCONNECTED;
		status->line = blocknum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}
	catch (InvalidOperationException^) {
		status->status = uart_message::DISCONNECTED;
		status->line = blocknum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}

	// Get the time at the end of the method
	auto time2 = std::chrono::high_resolution_clock::now();
	// Calculate the elapsed time
	std::chrono::duration<double> elapsed = time2 - time1;
	// Save the elpased time in the varible "time"
	time = elapsed.count();

	// Restore the old ReadTimeout
	p_serialPort->ReadTimeout = oldReadTimeout;

	// Save the status of the sending process in the status object and return the status
	if (block_status == uart_message::FINISHED) {
		status->status = uart_message::FINISHED;
		status->line = blocks;
	}
	else {
		status->status = error_type;
		status->line = blocknum + 1;
	}
	_finishedProcess = true;						//The sending process finished
	return status;
}

// The method for sending an encrypted bin file to the bootloader (asynchronous)
// After every sent packet of the encrypted file, the Desktop Application checks if there is a message from the bootloader, 
// if the process finished or an error occurred. When there is no error the next packet will be sent till all packets
// are sent. When an error occurs the packet will be sent again.
F103BootloaderDesktop::Sender::flash_result^ F103BootloaderDesktop::Sender::send_Encrypted_Bin_File_async(System::String^ path)
{
	// Get the time at the beginn of the method
	auto time1 = std::chrono::high_resolution_clock::now();

	// The flash_result object in which the result will be saved
	flash_result^ status = gcnew flash_result;
	status->status = uart_message::ACKN;
	status->line = -1;

	// Status of the current packet
	uart_message block_status = uart_message::PACKET_OK;
	// The error type of the last error
	uart_message error_type = uart_message::PACKET_OK;

	// Save the old ReadTimout of the serialport
	unsigned int oldReadTimeout = p_serialPort->ReadTimeout;

	// Load the file into a String array
	cli::array<String^>^ blockString = IO::File::ReadAllLines(path);

	// Load the String into unsigned char array, base64-decode
	cli::array<unsigned char>^ file = System::Convert::FromBase64String(blockString[0]);

	// Get the IV and the data
	cli::array<unsigned char>^ iv = gcnew cli::array<unsigned char>(16);
	cli::array<unsigned char>^ data = gcnew cli::array<unsigned char>(file->Length - 16);
	cli::array<unsigned char>::Copy(file, 0, iv, 0, 16);
	cli::array<unsigned char>::Copy(file, 16, data, 0, file->Length - 16);

	// Current packet number
	int blocknum = 0;
	// Calculate the number of blocks
	int size = data->Length - 16;
	int blocks = size / (received_packet_size + 16);

	p_serialPort->ReadTimeout = 10000;

	try {
		if (this->p_serialPort->ReadByte() == (unsigned char)uart_message::REQUEST_DATA) {
			// Generate a random synchronization message for synchronization after an error occurred
			Random^ rnd = gcnew Random();
			cli::array<byte>^ syncMessage = gcnew cli::array<byte>(8);
			rnd->NextBytes(syncMessage);

			// Send the synchronization message
			for (int i = 0; i < 8; i++) {
				p_serialPort->BaseStream->WriteByte(syncMessage[i]);
			}

			// Get the key of the encrypted firmware and send the key in encrypted form
			if (bootloaderVersion == 1)
				if (!sendKey()) {
					status->status = uart_message::CANCELLED;
					status->line = 1;
					return status;
				}

			// Send IV (not encrypted)
			for (int i = 0; i < 16; i++) {
				p_serialPort->BaseStream->WriteByte(iv[i]);
			}
			// Send size and start address (encrypted)
			for (int i = 0; i < 16; i++) {
				p_serialPort->BaseStream->WriteByte(data[i]);
			}

			// The sending process will be repeated as long as the status of the current packet is PACKET_OK
			while (block_status == uart_message::PACKET_OK) {
				// Check if there are packets to send
				if (blocknum < blocks) {

					// Send the 4 blocks to the Bootloader
					p_serialPort->BaseStream->Write(data, 16 + (received_packet_size + 16) * blocknum, received_packet_size);

					// Send the block with the crc32 checksum to the bootloader
					p_serialPort->BaseStream->Write(data, 16 + (received_packet_size + 16) * blocknum + received_packet_size, 16);

				}
				// Check if there is something to read from the Bootloader
				if ((p_serialPort->BytesToRead) == 0) {
					// There are no messages
					// Increase the current line number if not all lines have been sent
					if (blocknum < blocks) {
						blocknum++;
					}
				}
				else {
					// There is a message from the Bootloader
					// Read the message
					block_status = (uart_message)this->p_serialPort->ReadByte();

					// Check if maximum number of errors reached
					if (block_status != uart_message::FINISHED) {
						// Receive the packet number in which the error occurred and jump to this packet again
						String^ newPacket = this->p_serialPort->ReadLine();
						int newPacketNum = (Convert::ToInt32(newPacket));
						blocknum = newPacketNum;



						// Reset the packet status to PACKET_OK, so that the packet can be sent again
						if (block_status != uart_message::MAX_ERROR) {
							// Save the error_type
							error_type = block_status;
							block_status = uart_message::PACKET_OK;
						}
						// Send the synchronization-message to the Bootloader to synchronize the Bootloader
						// and the Desktop Application again, so that the sending process can be continued
						// The message can be chosen freely, but it has to be the same message like in the
						// bootloader.
						p_serialPort->BaseStream->Write(syncMessage, 0, 8);
						p_serialPort->Write("\n");
					}

				}
			}
		}
	}
	catch (TimeoutException^) {
		status->status = uart_message::GENERIC_ERROR;
		status->line = blocknum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}
	catch (IO::IOException^) {
		status->status = uart_message::DISCONNECTED;
		status->line = blocknum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}
	catch (InvalidOperationException^) {
		status->status = uart_message::DISCONNECTED;
		status->line = blocknum;
		_finishedProcess = false;						//The sending process didn't finished
		return status;
	}
	// Get the time at the end of the method
	auto time2 = std::chrono::high_resolution_clock::now();
	// Calculate the elapsed time
	std::chrono::duration<double> elapsed = time2 - time1;
	// Save the elpased time in the varible "time"
	time = elapsed.count();

	// Restore the old ReadTimeout
	p_serialPort->ReadTimeout = oldReadTimeout;

	// Save the status of the sending process in the status object and return the status
	if (block_status == uart_message::FINISHED) {
		status->status = uart_message::FINISHED;
		status->line = blocks;
	}
	else {
		status->status = error_type;
		status->line = blocknum + 1;
	}
	_finishedProcess = true;						//The sending process finished
	return status;
}

//The method for calculating the CRC32-checksum of given data
unsigned int F103BootloaderDesktop::Sender::crc32b(cli::array<unsigned char, 1>^ data, unsigned int num, unsigned int length) {
	int i, j;
	unsigned int byte, crc, mask;

	i = 0;
	crc = 0xFFFFFFFF;

	while (length) {
		length--;
		byte = data[num * received_packet_size + i];            // Get next byte.
		crc = crc ^ byte;
		for (j = 7; j >= 0; j--) {    // Do eight times.
			mask = -(crc & 1);
			crc = (crc >> 1) ^ (0xEDB88320 & mask);
		}
		i = i + 1;
	}
	return ~crc;
}
#pragma endregion

#pragma region Functions for getting information about the result
// The method for getting the result of the last sending process
cli::array<String^>^ F103BootloaderDesktop::Sender::getResult() {

	// Save the line and the status of the result into a String array and return the array
	cli::array<String^>^ p_result = gcnew cli::array<String^>{ Convert::ToString(result->line), Convert::ToString(result->status) };

	return p_result;

}

// The method for getting the information if the sending process ended properly
bool F103BootloaderDesktop::Sender::finishedProcess()
{
	return _finishedProcess;
}

// The method for getting the time duration of the last sending process
double F103BootloaderDesktop::Sender::getTime()
{
	return time;
}

// The method for getting the bootloader version
unsigned char F103BootloaderDesktop::Sender::getBootloaderVersion()
{
	return bootloaderVersion;
}

// The method for getting the packet size
unsigned int F103BootloaderDesktop::Sender::getPacketSize()
{
	return received_packet_size;
}
#pragma endregion

#pragma region Encryption
// The method for encryptiong a bin or hex firmware file. For bin files a header is added, which contains the IV,
// the start address and the file size. The CRC32-checksums are already included in the encrypted file. For hex file just
// the IV is added additionally.
void F103BootloaderDesktop::Sender::encryptFile(System::String^ path, cli::array<unsigned char>^ passwordBytes, unsigned int p_address, unsigned int packet_Size)
{
	// Get the file extension of the chosen file
	String^ fileExtension = path->Substring(path->LastIndexOf("."));

	// Check the file type
	if (String::Equals(fileExtension, ".bin", StringComparison::OrdinalIgnoreCase)) {
		// A bin file is selected

		// Load the file
		IO::BinaryReader^ binReader = gcnew System::IO::BinaryReader(System::IO::File::Open(path, System::IO::FileMode::Open));
		cli::array<unsigned char>^ binFile = binReader->ReadBytes(binReader->BaseStream->Length);
		binReader->Close();

		// Get random IV
		Random^ rnd = gcnew Random();
		cli::array<byte>^ IV = gcnew cli::array<byte>(16);
		rnd->NextBytes(IV);

		// Get size of the bin file
		int size = binFile->Length;
		String^ sizeHex = size.ToString("X");
		cli::array<unsigned char>^ sizeBytes = System::Text::Encoding::UTF8->GetBytes(sizeHex);

		// Get number of packets
		int packets = size / packet_Size;
		if (packets * packet_Size != size)
			packets++;

		// Reserve 8 bytes for the size
		cli::array<unsigned char>^ sizeBytes8 = gcnew cli::array<unsigned char>(8);
		cli::array<unsigned char>::Copy(sizeBytes, 0, sizeBytes8, 0, sizeBytes->Length);
		// Fill the remaining spaces of the 8 bytes with random data
		if (sizeBytes->Length < 8) {
			sizeBytes8[sizeBytes->Length] = '?';

			for (int i = sizeBytes->Length + 1; i < 8; i++) {
				sizeBytes8[i] = rnd->Next(0, 255);
			}
		}

		// Convert the start address for the bin file into hexadecimal format
		String^ addressHex = p_address.ToString("X");
		cli::array<unsigned char>^ addressBytes = System::Text::Encoding::UTF8->GetBytes(addressHex);

		// Reserve 8 bytes for the address
		cli::array<unsigned char>^ addressBytes8 = gcnew cli::array<unsigned char>(8);
		cli::array<unsigned char>::Copy(addressBytes, 0, addressBytes8, 0, addressBytes->Length);
		// Fill the remaining spaces of the 8 bytes with random data
		if (addressBytes->Length < 8) {
			addressBytes8[addressBytes->Length] = '?';

			for (int i = addressBytes->Length + 1; i < 8; i++) {
				addressBytes8[i] = rnd->Next(0, 255);
			}
		}

		// Add the size of the bin file and the start address to "fileToEncrypt"
		cli::array<unsigned char>^ fileToEncrypt = gcnew cli::array<unsigned char>(sizeBytes8->Length + addressBytes8->Length + packets * packet_Size + packets * 16);
		cli::array<unsigned char>::Copy(sizeBytes8, 0, fileToEncrypt, 0, sizeBytes8->Length);
		cli::array<unsigned char>::Copy(addressBytes8, 0, fileToEncrypt, sizeBytes8->Length, addressBytes8->Length);

		// Add the data and the associated CRC32-checksum to "fileToEncrypt"
		for (int i = 0; i < packets; i++) {

			int current_packet_size = 0;

			// Get packet size
			if (i != packets - 1) {
				current_packet_size = packet_Size;
			}
			else {
				current_packet_size = size % packet_Size;
				// When the result at the last packet is 0, the packet size is "packet_Size"
				if (current_packet_size == 0)
					current_packet_size += packet_Size;
			}
			addBlock(fileToEncrypt, binFile, i, current_packet_size, rnd->Next(), packet_Size);
		}

		// Encrypte the file
		cli::array<unsigned char>^ encryptedFile = myAES->AES_Encrypt(fileToEncrypt, passwordBytes, IV);

		// Convert the encrypted file into a base64-encoded String
		cli::array<unsigned char>^ resultBytes = gcnew cli::array<unsigned char>(IV->Length + encryptedFile->Length);
		cli::array<unsigned char>::Copy(IV, 0, resultBytes, 0, IV->Length);
		cli::array<unsigned char>::Copy(encryptedFile, 0, resultBytes, IV->Length, encryptedFile->Length);
		System::String^ result = System::Convert::ToBase64String(resultBytes);

		// Save the file
		IO::StreamWriter^ sw = gcnew IO::StreamWriter((path + ".crypt"), false);
		sw->Write(result);
		sw->Close();

	}
	else {
		// Load the file
		String^ hexFile = IO::File::ReadAllText(path);

		// Get random IV
		Random^ rnd = gcnew Random();
		cli::array<byte>^ IV = gcnew cli::array<byte>(16);
		rnd->NextBytes(IV);

		// Create an array from the string
		cli::array<unsigned char>^ fileToEncrypt = System::Text::Encoding::UTF8->GetBytes(hexFile);

		// Encrypt the file
		cli::array<unsigned char>^ encryptedFile = myAES->AES_Encrypt(fileToEncrypt, passwordBytes, IV);

		// Create a new array which contains the IV and the encrypted file
		cli::array<unsigned char>^ resultBytes = gcnew cli::array<unsigned char>(IV->Length + encryptedFile->Length);
		cli::array<unsigned char>::Copy(IV, 0, resultBytes, 0, IV->Length);
		cli::array<unsigned char>::Copy(encryptedFile, 0, resultBytes, IV->Length, encryptedFile->Length);

		// Base64-encoding
		System::String^ result = System::Convert::ToBase64String(resultBytes);

		// Save the file
		IO::StreamWriter^ sw = gcnew IO::StreamWriter((path + ".crypt"), false);
		sw->Write(result);
		sw->Close();
	}
}

// The method for encryptiong a bin or hex firmware file. This method gets called when no start address is specified.
// The default start address will be selected.
// Calls an overloaded function
void F103BootloaderDesktop::Sender::encryptFile(System::String^ path, cli::array<unsigned char>^ passwordBytes, unsigned int packet_Size)
{
	this->encryptFile(path, passwordBytes, 0x08006000u, packet_Size);
}

// The method for adding a bin packet and its CRC32-checksum to an array, which afterwards can be encrypted.
// When the last packet is smaller than the after packets, random data will be added
void F103BootloaderDesktop::Sender::addBlock(cli::array<unsigned char>^ fileArray, cli::array<unsigned char>^ dataArray, unsigned int num, unsigned int length, int seed, unsigned int packet_Size)
{
	// Create object for generating random characters for filling empty spaces
	Random^ rnd = gcnew Random(seed);

	// Get CRC32-checksum of the current block
	unsigned int crc = crc32b(dataArray, num, length);
	String^ crcHex = crc.ToString("X");
	cli::array<unsigned char>^ crcHexBytes = System::Text::Encoding::UTF8->GetBytes(crcHex);

	// Insert the checksum into a 16 byte block and fill the remaing bytes with random characters 
	cli::array<unsigned char>^ crcHexBytes16 = gcnew cli::array<unsigned char>(16);
	cli::array<unsigned char, 1>::Copy(crcHexBytes, 0, crcHexBytes16, 0, crcHexBytes->Length);

	// The '?' marks the end of the CRC32-checksum, the data after '?' is random data
	crcHexBytes16[crcHexBytes->Length] = '?';

	for (int i = crcHexBytes->Length + 1; i < 16; i++) {
		crcHexBytes16[i] = rnd->Next(97, 122);
	}

	// Copy the 64byte block and the crc block into the result string
	cli::array<unsigned char, 1>::Copy(dataArray, (int)(packet_Size * num), fileArray, (16 + ((packet_Size + 16) * num)), length);
	cli::array<unsigned char, 1>::Copy(crcHexBytes16, 0, fileArray, (16 + ((packet_Size + 16) * num) + packet_Size), 16);

	// Insert random data at the end of the last block, if it is smaller than the chosen packetsize
	if (length < packet_Size) {

		cli::array<unsigned char>^ fillStringBinBytes = gcnew cli::array<unsigned char>(packet_Size - length);

		// The '?' marks the end of the last bloc, the data after '?' is random data
		fillStringBinBytes[0] = '?';

		for (int i = 1; i < fillStringBinBytes->Length; i++) {
			fillStringBinBytes[i] = rnd->Next(97, 122);
		}

		// Copy the random data into the result string
		cli::array<unsigned char, 1>::Copy(fillStringBinBytes, 0, fileArray, (16 + ((packet_Size + 16) * num) + length), fillStringBinBytes->Length);
	}
}

#pragma region RSA Encryption
// The method for RSA encrypting a string
System::String^ F103BootloaderDesktop::Sender::RSA_Encrypt(System::String^ text) {
	// Get the bytes of the string
	cli::array<unsigned char>^ bytesToBeEncrypted = System::Text::Encoding::UTF8->GetBytes(text);
	// Encrypt
	cli::array<unsigned char>^ bytesEncrypted = myRSA->RSAEncrypt(bytesToBeEncrypted, false);
	// Base64-Encode the result
	System::String^ result = System::Convert::ToBase64String(bytesEncrypted);

	return result;
}

//The method for RSA decrypting a string
System::String^ F103BootloaderDesktop::Sender::RSA_Decrypt(System::String^ text) {
	// Base64-Decode the string and get the bytes
	cli::array<unsigned char>^ bytesToBeDecrypted = System::Convert::FromBase64String(text);
	// Decrypt the data
	cli::array<unsigned char>^ bytesDecrypted = myRSA->RSADecrypt(bytesToBeDecrypted, false);
	// Create a String from the decrypted data
	System::String^ result = System::Text::Encoding::Encoding::UTF8->GetString(bytesDecrypted);

	return result;
}

// The method for getting the parameters of the public key for RSA
cli::array<System::String^>^ F103BootloaderDesktop::Sender::getPublicKeyString()
{
	// Save parameters of the publica key
	RSAParameters RSAParams = myRSA->p_RSA->ExportParameters(false);

	// Serialize RSAParams, convert it to a String and save in "xml"
	auto sw = gcnew IO::StringWriter();
	auto xs = gcnew Xml::Serialization::XmlSerializer(RSAParameters::typeid);
	xs->Serialize(sw, RSAParams);
	String^ xml = sw->ToString();

	// Get the exponent of the public key
	int expFirst = xml->IndexOf("<Exponent>");
	int expLast = xml->IndexOf("</Exponent>");
	String^ exponent = xml->Substring(expFirst + 10, expLast - expFirst - 10);

	// Get the modulus of the public key
	int modFirst = xml->IndexOf("<Modulus>");
	int modLast = xml->IndexOf("</Modulus>");
	String^ modulus = xml->Substring(modFirst + 9, modLast - modFirst - 9);

	// Save the exponent and the modulus in an array
	cli::array<System::String^>^ result = gcnew cli::array<System::String^>(2);
	result[0] = exponent;
	result[1] = modulus;

	// Return the result array
	return result;
}

// The method for sending the parameters of the public key for RSA
bool F103BootloaderDesktop::Sender::sendKey()
{
	// Get the paramerters of the public key
	cli::array<System::String^>^ key = getPublicKeyString();

	// Get the exponent and the modulus
	String^ exponent = key[0];
	String^ modulus = key[1];

	// Convert the exponent and the modulus from Strings to arrays
	cli::array<unsigned char>^ expBytes = System::Convert::FromBase64String(exponent);
	cli::array<unsigned char>^ modBytes = System::Convert::FromBase64String(modulus);

	// Get length of exponent and modulus of RSA public key
	uint8_t expLen = expBytes->Length;
	uint8_t modLen = modBytes->Length;

	// Send the exponent length
	p_serialPort->BaseStream->WriteByte(expLen);

	// Send the exponent
	for (int i = 0; i < expLen; i++) {
		p_serialPort->BaseStream->WriteByte(expBytes[i]);
	}

	// Send the modulus length
	p_serialPort->BaseStream->WriteByte(modLen);

	// Send the modulus
	for (int i = 0; i < modLen; i++) {
		p_serialPort->BaseStream->WriteByte(modBytes[i]);
	}

	// Create 32 random bytes for STM32 random generator
	Random^ rnd = gcnew Random();
	cli::array<byte>^ bytesForSTM32Rand = gcnew cli::array<byte>(32);
	rnd->NextBytes(bytesForSTM32Rand);

	// Send the random bytes
	for (int i = 0; i < 32; i++) {
		p_serialPort->BaseStream->WriteByte(bytesForSTM32Rand[i]);
	}

	// Create an array for the encrypted data
	cli::array<unsigned char>^ encrypted = gcnew cli::array<unsigned char>(128);

	// Receive the encrypted data
	for (int i =0; i < 128; i++) {
		encrypted[i] = p_serialPort->BaseStream->ReadByte();
	}

	// Decrypt the encrypted data -> Temporary password
	cli::array<unsigned char>^ tempPasswordBytes = myRSA->RSADecrypt(encrypted, false);

	// Get the password for the file, open a dialog and ask for the password
	String^ realPassword = gcnew String("");
	PasswordForm^ dialogForm = gcnew PasswordForm();
	cli::array<unsigned char>^ realPasswordBytes = gcnew cli::array<unsigned char>(16);
	if (dialogForm->ShowDialog() == System::Windows::Forms::DialogResult::OK) {
		// Password was entered -> save the password
		realPasswordBytes = dialogForm->_passwordBytes;
	}
	else
		// The password was not entered -> Cancel the sending process
		return false;

	// Create a random IV
	cli::array<byte>^ iv_temp = gcnew cli::array<byte>(16);
	rnd->NextBytes(iv_temp);

	// Encrypt the password for the file with the temporary password
	cli::array<unsigned char>^ encryptedAESPassword = myAES->AES_Encrypt(realPasswordBytes, tempPasswordBytes, iv_temp);

	// Send the IV
	for (int i = 0; i < 16; i++) {
		p_serialPort->BaseStream->WriteByte(iv_temp[i]);
	}

	// Send the encrypted password
	for (int i = 0; i < 16; i++) {
		p_serialPort->BaseStream->WriteByte(encryptedAESPassword[i]);
	}
	return true;
}
#pragma endregion
#pragma endregion