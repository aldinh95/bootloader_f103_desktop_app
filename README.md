
# Bootloader_F103_V2


Desktop Application for the UART bootloader for STM32F103.

### Introduction

The Desktop Application for the bootloader for STM32F103 which sends HEX and bin files to the microcontroller.

### Bootloader

Link to the Bootloader firmware:
https://gitlab.com/aldinh95/bootloader_f103_v2

### Send modes

There are two different ways to send the file to the microcontroller

- **Synchronous**:

The application sends a HEX-line or bin-packet to the bootloader and waits for an answer from the microcontroller. Depending on the answer, the line or packet will be sent again, if an error occurred, or the next line or packet will be sent, if everthing was OK.

- **Asynchronous**:

The application starts sending the whole files, till the bootloader sends a message to the Desktop application. When an error occurred the line or packet will be sent again. When all lines or packets are sent, the application waits for the bootloader to send a "FINISHED"-message.

### Connecting to the bootloader

1. Select the right UART settings (COM port and baud rate).
2. Pressing the **Connect** button in the Desktop Application.
3. Resetting the microcontroller.

After connecting to the bootloader, the desktop application will receive some information about the configuration of the bootloader. So the Desktop Application can determine if encrypted firmware files can be received by the connected bootloader. Also the Desktop Application can decide if RSA encryption will be used for key exchange.

### Flashing the user application

1. Connect the Desktop Application to the Bootloader
- Connect the microcontroller to the PC
- Select the correct UART settings
- Click the "Connect" Button in the application
- Reset the microcontroller to connect the two devices

2. Select a firmware file
- For bin files the correct start address has to be specified in the textbox (otherwise the default start address will be chosen). The address has to be the address defined in the linker script (STM32F103C8Tx_FLASH.ld) and in system_stm32f1xx.c.
- Select whether the file will be sent synchronous or asynchronous

3. Click the "Send file" button
- The application will choose the right method for sending the file

4. If an encrypted file is sent and the connected bootloader supports RSA key exchange, the user has to enter the password, which was used for encryption. This password will be send in encrypted form to the bootloader.


### Encryption of firmware files

The desktop application can encrypt firmware files so that the data in the file cannot be read while it is being sent through UART. For encryption AES-CBC is used.

1. Select a HEX or bin file.
2. For bin files the packet size, which later will be used for sending, and the start address of the firmware has to be specified.
3. Select a 16 byte long password. If the bootloader is configured to support RSA key exchange, any password can be chosen. Otherwise the password, which was chosen in the bootloader firmware before compiling, has to be chosen.
4. Click the "Encrypt button"